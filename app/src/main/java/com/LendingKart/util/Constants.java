package com.LendingKart.util;

import com.LendingKart.model.Attachments;
import com.LendingKart.model.Director;

import java.util.ArrayList;

/**
 * Created by sharmaan on 07-09-2015.
 */
public class Constants {

    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";
    public static final String FORWARD_SLASH = "/";
    public static final String EQUAL_CHAR = "=";
    public static final String DOT = ".";
    public static final String EXCLAMATION_MARK = "!";
    public static final String COLLON = ":";
    public static final String COMMA = ",";
    public static final String INTENT_VALUE = "position";
    public static final String NAVIGATION_VALUE = "navigation";

    //FOR ATTACHMENTS
    public static final String IMPORTANT_DOCUMENT = "importantDocument";
    public static final String KYC = "kyc";
    public static final ArrayList<String> VAT_RETURNS = new ArrayList<>();
    public static final ArrayList<String> BANK_STATEMENT_COMPANY = new ArrayList<>();
    public static final ArrayList<String> BANK_STATEMENT_DIRECTOR = new ArrayList<>();
    public static final ArrayList<String> PAN_CARD_PERSONAL = new ArrayList<>();
    public static final ArrayList<String> AADHAR_CARD_PERSONAL = new ArrayList<>();
    public static final ArrayList<String> DRIVING_LICENCE_PERSONAL = new ArrayList<>();
    public static final ArrayList<String> PASSPORT_PERSONAL = new ArrayList<>();
    public static final ArrayList<String> VOTER_ID_PERSONAL = new ArrayList<>();
    public static final ArrayList<String> INCOME_TAX_RETURNS_PERSONAL = new ArrayList<>();
    public static final ArrayList<String> PAN_CARD_COMPANY = new ArrayList<>();
    public static final ArrayList<String> INCOME_TAX_RETURNS_COMPANY = new ArrayList<>();

    //ATTACHMENTS
    public static final Attachments mAttachments = new Attachments();
    public static final Attachments.ImportantDocument mImportantDocuments = mAttachments.new ImportantDocument();
    public static final Attachments.Kyc mKyc = mAttachments.new Kyc();

    //PERSONAL DETAILS
    public static final Director DIRECTOR = new Director();
    public static final Director.YourPersonalDetails YOUR_PERSONAL_DETAILS = DIRECTOR.new YourPersonalDetails();
    public static final Director.AnnualIncomeDetails ANNUAL_INCOME_DETAILS = DIRECTOR.new AnnualIncomeDetails();
    public static final Director.PersonalAddress PERSONAL_ADDRESS = DIRECTOR.new PersonalAddress();
    public static final Director.EducationDetails EDUCATION_DETAILS = DIRECTOR.new EducationDetails();
    public static final Director.MyFamilyDetails MY_FAMILY_DETAILS = DIRECTOR.new MyFamilyDetails();
    public static final Director.SocialDetails SOCIAL_DETAILS = DIRECTOR.new SocialDetails();
}
