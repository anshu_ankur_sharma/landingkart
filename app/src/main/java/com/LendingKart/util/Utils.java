package com.LendingKart.util;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Patterns;

/**
 * Created by sharmaan on 07-09-2015.
 */
public class Utils {

    /**
     * Check if a string is empty or null
     *
     * @param str - input string
     * @return false if string is empty or null; true otherwise
     */
    public static boolean isEmpty(String str) {
        return !(str != null && !(str.trim()).equals(Constants.EMPTY_STRING));
    }

    public static int getDpFromPixels(int pixel, Context context) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pixel / scale);
    }

    public static boolean isEmptyWithoutTrim(String str) {
        if (str != null && (str.length() > 0)) {
            return false;
        }
        return true;
    }

    /**
     * method to check email and redirecting user to next screen
     *
     * @param input entered email address
     * @return true id input is valid else false
     */
    public static boolean validateEmailAddress(String input) {
        return Patterns.EMAIL_ADDRESS.matcher(input).matches();
    }

    /**
     * This method convets dp unit to equivalent device specific value in
     * pixels.
     *
     * @param dp      A value in dp(Device independent pixels) unit. Which we need
     *                to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A int value to represent Pixels equivalent to dp according to
     * device
     */
    public static int getPixelFromDP(int dp, Context context) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    public static boolean hasConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }
    //created by Anil
    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return  dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp){
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }
}
