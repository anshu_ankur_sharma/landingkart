package com.LendingKart.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.LendingKart.model.BusinessDetail;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by sharmaan on 21-09-2015.
 */
public class AppPreferences {

    private static final String APP_SHARED_PREFS = AppPreferences.class
            .getSimpleName();
    private SharedPreferences _sharedPrefs;
    private SharedPreferences.Editor _prefsEditor;

    public static final String KEY_BUSINESS_DETAIL = "businessdetail";
    public static final String KEY_BUSINESSDETAIL_PERCENTAGE = "businessdetailPercentage";
    public static final String KEY_PERSONALDETAIL_PERCENTAGE = "personaldetailPercentage";
    public static final String KEY_ATTACHMENTDETAIL_PERCENTAGE = "attachmentdetailPercentage";

    public AppPreferences(Context context) {

        this._sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS,
                Activity.MODE_PRIVATE);
        this._prefsEditor = _sharedPrefs.edit();

    }

    public void setBusinessDetail(BusinessDetail businessDetail) {
        Gson gson = new Gson();
        String json = gson.toJson(businessDetail);
        _prefsEditor.putString(KEY_BUSINESS_DETAIL, json).commit();
    }

    public BusinessDetail getBusinessDetail() {
        BusinessDetail businessDetail = new BusinessDetail();
        Gson gson = new Gson();
        String json = _sharedPrefs.getString(KEY_BUSINESS_DETAIL, "");

        businessDetail = gson.fromJson(json,
                BusinessDetail.class);
        return businessDetail;

    }

    public void setBusinessdetailPercentage(int completion) {

        _prefsEditor.putInt(KEY_BUSINESSDETAIL_PERCENTAGE, completion).commit();
    }

    public int getBusinessdetailPercentage() {
        int value = _sharedPrefs.getInt(KEY_BUSINESSDETAIL_PERCENTAGE, 0);
        return value;
    }

    public void setPersonaldetailPercentage(int completion) {

        _prefsEditor.putInt(KEY_PERSONALDETAIL_PERCENTAGE, completion).commit();
    }

    public int getPersonaldetailPercentage() {
        int value = _sharedPrefs.getInt(KEY_PERSONALDETAIL_PERCENTAGE, 0);
        return value;
    }

    public void setAttachmentPercentage(int completion) {

        _prefsEditor.putInt(KEY_ATTACHMENTDETAIL_PERCENTAGE, completion).commit();
    }

    public int getAttachmnetPercentage() {
        int value = _sharedPrefs.getInt(KEY_ATTACHMENTDETAIL_PERCENTAGE, 0);
        return value;
    }
}
