package com.LendingKart.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.RadioButton;

import com.LendingKart.R;

/**
 * Created by chakrabo on 9/15/2015.
 */
public class TresbuRadioButton extends RadioButton {

    private static final String TAG = "CustomFont";

    public TresbuRadioButton(Context context) {
        super(context);
    }

    public TresbuRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public TresbuRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TresbuEditText);
        String customFont = a.getString(R.styleable.TresbuEditText_edit_text_font);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), asset);
        } catch (Exception e) {

            return false;
        }
        setTypeface(tf);
        return true;
    }

}