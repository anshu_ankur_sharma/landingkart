/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */

package com.LendingKart.customviews;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.ScrollView;

/**
 * Common navigation drawer to be used across newshunt application
 *
 * Created by sharmaan on 07-09-2015.
 */
public class NavigationDrawer extends ScrollView {

    public NavigationDrawer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public NavigationDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NavigationDrawer(Context context) {
        super(context);
        init();
    }


    private void init() {

    }

    private void initSettingTopMenu(LinearLayout rootView) {

    }

    /**
     * Method to update text on sign in and sign out button
     */
    private void updateSignInOutText() {

    }

    private void initSettingMidMenu(LinearLayout rootView) {

    }


    private void initSettingBottomMenu(LinearLayout rootView) {

    }

    public void setNavigationDrawer(final Activity activity, DrawerLayout drawerLayout,
                                    Toolbar toolbar, boolean actionBarToggleRequired) {


    }


}
