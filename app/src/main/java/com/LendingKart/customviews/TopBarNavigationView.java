/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */

package com.LendingKart.customviews;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.LendingKart.R;

/**
 * Class to set toolbar option view.
 * <p/>
 * Created by sharmaan on 07-09-2015.
 */
public class TopBarNavigationView extends RelativeLayout {

    public TopBarNavigationView(Context context) {
        super(context);
    }

    public void setToolbarOptionView(final Context context, int viewType) {

        final RelativeLayout rootView =
                (RelativeLayout) LayoutInflater.from(getContext())
                        .inflate(R.layout.actionbar_options, this, true);

        ImageView ivBusinessDetail = (ImageView) rootView.findViewById(R.id.ivBusinessDetail);
        ImageView ivPersonalDetail = (ImageView) rootView.findViewById(R.id.ivPersonalDetail);
        ImageView ivAttachment = (ImageView) rootView.findViewById(R.id.ivAttachment);

        switch (viewType) {
            case 0:
                ivBusinessDetail.setImageDrawable(getResources().getDrawable(R.drawable.businessdetail));
                ivPersonalDetail.setImageDrawable(getResources().getDrawable(R.drawable.personaldetail));
                ivAttachment.setImageDrawable(getResources().getDrawable(R.drawable.attachment));
                break;
            case 1:
                ivBusinessDetail.setImageDrawable(getResources().getDrawable(R.drawable.businessdetail));
                ivPersonalDetail.setImageDrawable(
                        getResources().getDrawable(R.drawable.personaldetail));
                ivAttachment.setImageDrawable(getResources().getDrawable(R.drawable.attachment));
                break;
            case 2:
                ivBusinessDetail.setImageDrawable(getResources().getDrawable(R.drawable.businessdetail));
                ivPersonalDetail.setImageDrawable(
                        getResources().getDrawable(R.drawable.personaldetail));
                ivAttachment.setImageDrawable(getResources().getDrawable(R.drawable.attachment));
                break;
            default:
                break;
        }


        LinearLayout llBusinessDetail = (LinearLayout) rootView.findViewById(R.id
                .llBusinessDetail);
        llBusinessDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        LinearLayout llPersonalDetail = (LinearLayout) rootView.findViewById(R.id
                .llPersonalDetail);
        llPersonalDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        LinearLayout llAttachment = (LinearLayout) rootView.findViewById(R.id
                .llAttachment);
        llAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }

        });

    }


}
