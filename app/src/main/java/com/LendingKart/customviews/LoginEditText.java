package com.LendingKart.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

import com.LendingKart.R;

/**
 * Created by chakrabo on 9/30/2015.
 */
public class LoginEditText extends EditText
{

    private static final String TAG = "CustomFont";

    public LoginEditText(Context context) {
        super(context);
    }

    public LoginEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public LoginEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TresbuEditText);
        String customFont = a.getString(R.styleable.TresbuEditText_edit_text_font);
        setCustomFont(ctx, customFont);
        a.recycle();
    }
    private KeyImeChange keyImeChangeListener;
    public void setKeyImeChangeListener(KeyImeChange listener){
        keyImeChangeListener = listener;
    }

    public interface KeyImeChange {
        public void onKeyIme(int keyCode, KeyEvent event);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            // dispatchKeyEvent(event);
            keyImeChangeListener.onKeyIme(keyCode, event);
            return false;
        }
        return super.onKeyPreIme(keyCode, event);
    }
    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), asset);
        } catch (Exception e) {

            return false;
        }
        setTypeface(tf);
        return true;
    }

}