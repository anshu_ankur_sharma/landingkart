package com.LendingKart.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.LendingKart.R;
import com.LendingKart.customviews.TresbuEditText;
import com.LendingKart.util.Constants;
import com.google.gson.Gson;

/**
 * Created by sharmaan on 12-09-2015.
 */
public class PersonalDetailFragment extends Fragment {
    View mRootView;

    LinearLayout ll_director, ll_director2, ll_director3, ll_director4, ll_your_personal_details, ll_annual_income_details, ll_social_details, ll_my_family_details, ll_personal_address, ll_education_details;

    //FOR DIRECTOR 2
    LinearLayout ll_your_personal_details2, ll_annual_income_details2, ll_social_details2, ll_my_family_details2, ll_personal_address2, ll_education_details2;

    //FOR DIRECTOR 3
    LinearLayout ll_your_personal_details3, ll_annual_income_details3, ll_social_details3, ll_my_family_details3, ll_personal_address3, ll_education_details3;
    //FOR DIRECTOR 4
    LinearLayout ll_your_personal_details4, ll_annual_income_details4, ll_social_details4, ll_my_family_details4, ll_personal_address4, ll_education_details4;

    RelativeLayout director_layout, director_layout2, director_layout3, director_layout4, your_personal_details_layout, annual_income_details_layout, social_details_layout, my_family_details_layout, personal_address_layout, education_details_layout, residential_address_layout, permanent_address_layout;

    // DIRECTOR 2
    RelativeLayout your_personal_details_layout2, annual_income_details_layout2, social_details_layout2, my_family_details_layout2, personal_address_layout2, education_details_layout2, residential_address_layout2, permanent_address_layout2;

    //FOR DIRECTOR 3
    RelativeLayout your_personal_details_layout3, annual_income_details_layout3, social_details_layout3, my_family_details_layout3, personal_address_layout3, education_details_layout3, residential_address_layout3, permanent_address_layout3;

    //FOR DIRECTOR 4
    RelativeLayout your_personal_details_layout4, annual_income_details_layout4, social_details_layout4, my_family_details_layout4, personal_address_layout4, education_details_layout4, residential_address_layout4, permanent_address_layout4;

    RelativeLayout view_appear = null, director_view_appear = null, director_view_appear2 = null, director_view_appear3 = null, director_view_appear4 = null;

    ToggleButton tb_residential_address, tb_permanent_address;

    CheckBox cb_facebook, cb_linkedin, cb_twitter, cb_others;

    /***********************************
     * @@@@@@@@@ STATUS IMAGES @@@@@@
     **********************************/
    ImageView iv_Status_director, iv_Status_your_personal_details, iv_Status_annual_income_details, iv_Status_personal_address,
            iv_Status_education_details, iv_Status_my_family_details, iv_Status_social_details;


    /*****************************
     * @@@@@@@@ VISIBILITY IMAGES
     ****************************/
    ImageView iv_Visibility_director, iv_Visibility_director2, iv_Visibility_director3, iv_Visibility_director4, iv_Visibility_your_personal_details, iv_Visibility_annual_income_details, iv_Visibility_personal_address,
            iv_Visibility_education_details, iv_Visibility_my_family_details, iv_Visibility_social_details;

    ImageView iv_Visibility_your_personal_details2, iv_Visibility_annual_income_details2, iv_Visibility_personal_address2,
            iv_Visibility_education_details2, iv_Visibility_my_family_details2, iv_Visibility_social_details2;

    ImageView iv_Visibility_your_personal_details3, iv_Visibility_annual_income_details3, iv_Visibility_personal_address3,
            iv_Visibility_education_details3, iv_Visibility_my_family_details3, iv_Visibility_social_details3;

    ImageView iv_Visibility_your_personal_details4, iv_Visibility_annual_income_details4, iv_Visibility_personal_address4,
            iv_Visibility_education_details4, iv_Visibility_my_family_details4, iv_Visibility_social_details4;

    ImageView iv_personals_visibility = null, iv_director_visibility = null;
    ImageView iv_personals_visibility2 = null, iv_director_visibility2 = null;
    ImageView iv_personals_visibility3 = null, iv_director_visibility3 = null;
    ImageView iv_personals_visibility4 = null, iv_director_visibility4 = null;

    ImageView iv_facebook;

    RelativeLayout rl_facebook, rl_linkedin, rl_twitter, rl_others;
    View view_facebook, view_linkedin, view_twitter, view_others;

    //DIRECTOR 1
    TresbuEditText et_full_name, et_mobile_no, et_email_id, et_dob, et_facebook, et_linkedin, et_twitter, et_others, et_residential_address_pin_code,
            et_residential_address_house_no_and_locality, et_residential_address_city, et_residential_address_state, et_permanent_address_pin_code,
            et_pernanent_address_house_no_and_locality, et_permanent_address_city, et_permanent_address_state;
    RadioGroup rg_annual_income, rg_permanent_address_residential_premises;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mRootView = inflater
                .inflate(R.layout.activity_personals, container, false);
        initialiseIds(mRootView);
        iv_personals_visibility = iv_Visibility_director;
        iv_director_visibility = iv_Visibility_your_personal_details;
        iv_director_visibility2 = iv_Visibility_your_personal_details2;
        iv_director_visibility3 = iv_Visibility_your_personal_details3;
        iv_director_visibility4 = iv_Visibility_your_personal_details4;

        //DIRECTOR 1
        ll_director.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (director_layout.getVisibility() == View.VISIBLE) {
                    director_layout.setVisibility(View.GONE);
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (view_appear != null) {
                        view_appear.setVisibility(View.GONE);
                        iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    director_layout.setVisibility(View.VISIBLE);
                    view_appear = director_layout;
                    iv_personals_visibility = iv_Visibility_director;
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        //DIRECTOR 2
        ll_director2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (director_layout2.getVisibility() == View.VISIBLE) {
                    director_layout2.setVisibility(View.GONE);
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (view_appear != null) {
                        view_appear.setVisibility(View.GONE);
                        iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    director_layout2.setVisibility(View.VISIBLE);
                    view_appear = director_layout2;
                    iv_personals_visibility = iv_Visibility_director2;
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        //DIRECTOR 3
        ll_director3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (director_layout3.getVisibility() == View.VISIBLE) {
                    director_layout3.setVisibility(View.GONE);
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (view_appear != null) {
                        view_appear.setVisibility(View.GONE);
                        iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    director_layout3.setVisibility(View.VISIBLE);
                    view_appear = director_layout3;
                    iv_personals_visibility = iv_Visibility_director3;
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        //DIRECTOR 4
        ll_director4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (director_layout4.getVisibility() == View.VISIBLE) {
                    director_layout4.setVisibility(View.GONE);
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (view_appear != null) {
                        view_appear.setVisibility(View.GONE);
                        iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    director_layout4.setVisibility(View.VISIBLE);
                    view_appear = director_layout4;
                    iv_personals_visibility = iv_Visibility_director4;
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        // FOR DIRECTOR 1
        director1OnclickListener();
        director1PersonalDetailsChangeListener();

        // FOR DIRECTOR 2
        director2OnclickListener();

        //FOR DIRECTOR 3
        director3OnclickListner();

        //FOR DIRECTOR 4
        director4OnclickListner();

        //


        return mRootView;
    }

    public void initialiseIds(View mRootView) {
        ll_director = (LinearLayout) mRootView.findViewById(R.id.ll_director);
        ll_director2 = (LinearLayout) mRootView.findViewById(R.id.ll_director2);
        ll_director3 = (LinearLayout) mRootView.findViewById(R.id.ll_director3);
        ll_director4 = (LinearLayout) mRootView.findViewById(R.id.ll_director4);


        ll_your_personal_details = (LinearLayout) mRootView.findViewById(R.id.ll_your_personal_details);
        ll_annual_income_details = (LinearLayout) mRootView.findViewById(R.id.ll_annual_income_details);
        ll_social_details = (LinearLayout) mRootView.findViewById(R.id.ll_social_details);
        ll_my_family_details = (LinearLayout) mRootView.findViewById(R.id.ll_my_family_details);
        ll_personal_address = (LinearLayout) mRootView.findViewById(R.id.ll_personal_address);
        ll_education_details = (LinearLayout) mRootView.findViewById(R.id.ll_education_details);

        //DIRECTOR 2
        ll_your_personal_details2 = (LinearLayout) mRootView.findViewById(R.id.ll_your_personal_details2);
        ll_annual_income_details2 = (LinearLayout) mRootView.findViewById(R.id.ll_annual_income_details2);
        ll_social_details2 = (LinearLayout) mRootView.findViewById(R.id.ll_social_details2);
        ll_my_family_details2 = (LinearLayout) mRootView.findViewById(R.id.ll_my_family_details2);
        ll_personal_address2 = (LinearLayout) mRootView.findViewById(R.id.ll_personal_address2);
        ll_education_details2 = (LinearLayout) mRootView.findViewById(R.id.ll_education_details2);

        //FOR DIRECTOR 3
        ll_your_personal_details3 = (LinearLayout) mRootView.findViewById(R.id.ll_your_personal_details3);
        ll_annual_income_details3 = (LinearLayout) mRootView.findViewById(R.id.ll_annual_income_details3);
        ll_social_details3 = (LinearLayout) mRootView.findViewById(R.id.ll_social_details3);
        ll_my_family_details3 = (LinearLayout) mRootView.findViewById(R.id.ll_my_family_details3);
        ll_personal_address3 = (LinearLayout) mRootView.findViewById(R.id.ll_personal_address3);
        ll_education_details3 = (LinearLayout) mRootView.findViewById(R.id.ll_education_details3);

        //FOR DIRECTOR 4
        ll_your_personal_details4 = (LinearLayout) mRootView.findViewById(R.id.ll_your_personal_details4);
        ll_annual_income_details4 = (LinearLayout) mRootView.findViewById(R.id.ll_annual_income_details4);
        ll_social_details4 = (LinearLayout) mRootView.findViewById(R.id.ll_social_details4);
        ll_my_family_details4 = (LinearLayout) mRootView.findViewById(R.id.ll_my_family_details4);
        ll_personal_address4 = (LinearLayout) mRootView.findViewById(R.id.ll_personal_address4);
        ll_education_details4 = (LinearLayout) mRootView.findViewById(R.id.ll_education_details4);

        director_layout = (RelativeLayout) mRootView.findViewById(R.id.director_layout);
        director_layout2 = (RelativeLayout) mRootView.findViewById(R.id.director_layout2);
        director_layout3 = (RelativeLayout) mRootView.findViewById(R.id.director_layout3);
        director_layout4 = (RelativeLayout) mRootView.findViewById(R.id.director_layout4);

        your_personal_details_layout = (RelativeLayout) mRootView.findViewById(R.id.your_personal_details_layout);
        annual_income_details_layout = (RelativeLayout) mRootView.findViewById(R.id.annual_income_details_layout);
        social_details_layout = (RelativeLayout) mRootView.findViewById(R.id.social_details_layout);
        my_family_details_layout = (RelativeLayout) mRootView.findViewById(R.id.my_family_details_layout);
        personal_address_layout = (RelativeLayout) mRootView.findViewById(R.id.personal_address_layout);
        education_details_layout = (RelativeLayout) mRootView.findViewById(R.id.education_details_layout);
        residential_address_layout = (RelativeLayout) mRootView.findViewById(R.id.residential_address_layout);
        permanent_address_layout = (RelativeLayout) mRootView.findViewById(R.id.permanent_address_layout);

        //FOR DIRECTOR 2
        your_personal_details_layout2 = (RelativeLayout) mRootView.findViewById(R.id.your_personal_details_layout2);
        annual_income_details_layout2 = (RelativeLayout) mRootView.findViewById(R.id.annual_income_details_layout2);
        social_details_layout2 = (RelativeLayout) mRootView.findViewById(R.id.social_details_layout2);
        my_family_details_layout2 = (RelativeLayout) mRootView.findViewById(R.id.my_family_details_layout2);
        personal_address_layout2 = (RelativeLayout) mRootView.findViewById(R.id.personal_address_layout2);
        education_details_layout2 = (RelativeLayout) mRootView.findViewById(R.id.education_details_layout2);
        // residential_address_layout2 = (RelativeLayout) mRootView.findViewById(R.id.residential_address_layout2);
        // permanent_address_layout2 = (RelativeLayout) mRootView.findViewById(R.id.permanent_address_layout2);

        //FOR DIRECTOR 3
        your_personal_details_layout3 = (RelativeLayout) mRootView.findViewById(R.id.your_personal_details_layout3);
        annual_income_details_layout3 = (RelativeLayout) mRootView.findViewById(R.id.annual_income_details_layout3);
        social_details_layout3 = (RelativeLayout) mRootView.findViewById(R.id.social_details_layout3);
        my_family_details_layout3 = (RelativeLayout) mRootView.findViewById(R.id.my_family_details_layout3);
        personal_address_layout3 = (RelativeLayout) mRootView.findViewById(R.id.personal_address_layout3);
        education_details_layout3 = (RelativeLayout) mRootView.findViewById(R.id.education_details_layout3);
        // residential_address_layout3 = (RelativeLayout) mRootView.findViewById(R.id.residential_address_layout3);
        // permanent_address_layout3 = (RelativeLayout) mRootView.findViewById(R.id.permanent_address_layout3);

        //FOR DIRECTOR 4
        your_personal_details_layout4 = (RelativeLayout) mRootView.findViewById(R.id.your_personal_details_layout4);
        annual_income_details_layout4 = (RelativeLayout) mRootView.findViewById(R.id.annual_income_details_layout4);
        social_details_layout4 = (RelativeLayout) mRootView.findViewById(R.id.social_details_layout4);
        my_family_details_layout4 = (RelativeLayout) mRootView.findViewById(R.id.my_family_details_layout4);
        personal_address_layout4 = (RelativeLayout) mRootView.findViewById(R.id.personal_address_layout4);
        education_details_layout4 = (RelativeLayout) mRootView.findViewById(R.id.education_details_layout4);
        // residential_address_layout4 = (RelativeLayout) mRootView.findViewById(R.id.residential_address_layout4);
        //permanent_address_layout4 = (RelativeLayout) mRootView.findViewById(R.id.permanent_address_layout4);


        tb_residential_address = (ToggleButton) mRootView.findViewById(R.id.tb_residential_address);
        tb_permanent_address = (ToggleButton) mRootView.findViewById(R.id.tb_permanent_address);


        /**** @@@@@@@@@@@@@@ STATUS VIEW @@@@@@@@@ ***********/
        iv_Status_director = (ImageView) mRootView.findViewById(R.id.iv_Status_director);
        // iv_Status_director1 = (ImageView) mRootView.findViewById(R.id.iv_Status_director1);
        iv_Status_your_personal_details = (ImageView) mRootView.findViewById(R.id.iv_Status_your_personal_details);
        iv_Status_annual_income_details = (ImageView) mRootView.findViewById(R.id.iv_Status_annual_income_details);
        iv_Status_personal_address = (ImageView) mRootView.findViewById(R.id.iv_Status_personal_address);
        iv_Status_education_details = (ImageView) mRootView.findViewById(R.id.iv_Status_education_details);
        iv_Status_my_family_details = (ImageView) mRootView.findViewById(R.id.iv_Status_my_family_details);
        iv_Status_social_details = (ImageView) mRootView.findViewById(R.id.iv_Status_social_details);

        /***********  @@@@@@@@@@ VISIBILITY IMAGES @@@@@@@@@ ************/
        iv_Visibility_director = (ImageView) mRootView.findViewById(R.id.iv_Visibility_director);
        iv_Visibility_director2 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_director2);
        iv_Visibility_director3 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_director3);
        iv_Visibility_director4 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_director4);

        iv_Visibility_your_personal_details = (ImageView) mRootView.findViewById(R.id.iv_Visibility_your_personal_details);
        iv_Visibility_annual_income_details = (ImageView) mRootView.findViewById(R.id.iv_Visibility_annual_income_details);
        iv_Visibility_personal_address = (ImageView) mRootView.findViewById(R.id.iv_Visibility_personal_address);
        iv_Visibility_education_details = (ImageView) mRootView.findViewById(R.id.iv_Visibility_education_details);
        iv_Visibility_my_family_details = (ImageView) mRootView.findViewById(R.id.iv_Visibility_my_family_details);
        iv_Visibility_social_details = (ImageView) mRootView.findViewById(R.id.iv_Visibility_social_details);

        iv_Visibility_your_personal_details2 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_your_personal_details2);
        iv_Visibility_annual_income_details2 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_annual_income_details2);
        iv_Visibility_personal_address2 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_personal_address2);
        iv_Visibility_education_details2 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_education_details2);
        iv_Visibility_my_family_details2 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_my_family_details2);
        iv_Visibility_social_details2 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_social_details2);

        iv_Visibility_your_personal_details3 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_your_personal_details3);
        iv_Visibility_annual_income_details3 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_annual_income_details3);
        iv_Visibility_personal_address3 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_personal_address3);
        iv_Visibility_education_details3 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_education_details3);
        iv_Visibility_my_family_details3 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_my_family_details3);
        iv_Visibility_social_details3 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_social_details3);

        iv_Visibility_your_personal_details4 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_your_personal_details4);
        iv_Visibility_annual_income_details4 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_annual_income_details4);
        iv_Visibility_personal_address4 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_personal_address4);
        iv_Visibility_education_details4 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_education_details4);
        iv_Visibility_my_family_details4 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_my_family_details4);
        iv_Visibility_social_details4 = (ImageView) mRootView.findViewById(R.id.iv_Visibility_social_details4);

        /************@@@@@@@@@@@@@ SOCIAL TEXT VIEWS  @@@@@@ *******/
        cb_facebook = (CheckBox) mRootView.findViewById(R.id.cb_facebook);
        cb_linkedin = (CheckBox) mRootView.findViewById(R.id.cb_linkedin);
        cb_twitter = (CheckBox) mRootView.findViewById(R.id.cb_twitter);
        cb_others = (CheckBox) mRootView.findViewById(R.id.cb_others);

        rl_facebook = (RelativeLayout) mRootView.findViewById(R.id.rl_facebook);
        rl_linkedin = (RelativeLayout) mRootView.findViewById(R.id.rl_linkedin);
        rl_twitter = (RelativeLayout) mRootView.findViewById(R.id.rl_twitter);
        rl_others = (RelativeLayout) mRootView.findViewById(R.id.rl_others);

        view_facebook = (View) mRootView.findViewById(R.id.view_facebook);
        view_linkedin = (View) mRootView.findViewById(R.id.view_linkedin);
        view_twitter = (View) mRootView.findViewById(R.id.view_twitter);
        view_others = (View) mRootView.findViewById(R.id.view_others);

        et_full_name = (TresbuEditText) mRootView.findViewById(R.id.et_full_name);
        et_mobile_no = (TresbuEditText) mRootView.findViewById(R.id.et_mobile_no);
        et_email_id = (TresbuEditText) mRootView.findViewById(R.id.et_email_id);
        et_dob = (TresbuEditText) mRootView.findViewById(R.id.et_dob);

        rg_annual_income = (RadioGroup) mRootView.findViewById(R.id.rg_annual_income);

        et_facebook = (TresbuEditText) mRootView.findViewById(R.id.et_facebook);
        et_linkedin = (TresbuEditText) mRootView.findViewById(R.id.et_linkedin);
        et_twitter = (TresbuEditText) mRootView.findViewById(R.id.et_twitter);
        et_others = (TresbuEditText) mRootView.findViewById(R.id.et_others);

        et_residential_address_pin_code = (TresbuEditText) mRootView.findViewById(R.id.et_residential_address_pin_code);
        et_residential_address_house_no_and_locality = (TresbuEditText) mRootView.findViewById(R.id.et_residential_address_house_no_and_locality);
        et_residential_address_city = (TresbuEditText) mRootView.findViewById(R.id.et_residential_address_city);
        et_residential_address_state = (TresbuEditText) mRootView.findViewById(R.id.et_residential_address_state);

        et_permanent_address_pin_code = (TresbuEditText) mRootView.findViewById(R.id.et_permanent_address_pin_code);
        et_pernanent_address_house_no_and_locality = (TresbuEditText) mRootView.findViewById(R.id.et_pernanent_address_house_no_and_locality);
        et_permanent_address_city = (TresbuEditText) mRootView.findViewById(R.id.et_permanent_address_city);
        et_permanent_address_state = (TresbuEditText) mRootView.findViewById(R.id.et_permanent_address_state);

        rg_permanent_address_residential_premises = (RadioGroup) mRootView.findViewById(R.id.rg_permanent_address_residential_premises);
    }

    public void director1OnclickListener() {
        ll_your_personal_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    your_personal_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = your_personal_details_layout;
                    iv_director_visibility = iv_Visibility_your_personal_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_annual_income_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (annual_income_details_layout.getVisibility() == View.VISIBLE) {
                    annual_income_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    annual_income_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = annual_income_details_layout;
                    iv_director_visibility = iv_Visibility_annual_income_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_social_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (social_details_layout.getVisibility() == View.VISIBLE) {
                    social_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    social_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = social_details_layout;
                    iv_director_visibility = iv_Visibility_social_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_my_family_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_family_details_layout.getVisibility() == View.VISIBLE) {
                    my_family_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    my_family_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = my_family_details_layout;
                    iv_director_visibility = iv_Visibility_my_family_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_personal_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (personal_address_layout.getVisibility() == View.VISIBLE) {
                    personal_address_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    personal_address_layout.setVisibility(View.VISIBLE);
                    director_view_appear = personal_address_layout;
                    iv_director_visibility = iv_Visibility_personal_address;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_education_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (education_details_layout.getVisibility() == View.VISIBLE) {
                    education_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    education_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = education_details_layout;
                    iv_director_visibility = iv_Visibility_education_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        tb_residential_address.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    residential_address_layout.setVisibility(View.GONE);
                } else {
                    residential_address_layout.setVisibility(View.VISIBLE);
                }

            }
        });
        tb_permanent_address.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    permanent_address_layout.setVisibility(View.GONE);
                } else {
                    permanent_address_layout.setVisibility(View.VISIBLE);
                }

            }
        });

        cb_facebook.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rl_facebook.setVisibility(View.VISIBLE);
                    view_facebook.setVisibility(View.VISIBLE);
                } else {
                    rl_facebook.setVisibility(View.GONE);
                    view_facebook.setVisibility(View.GONE);
                }

            }
        });

        cb_linkedin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rl_linkedin.setVisibility(View.VISIBLE);
                    view_linkedin.setVisibility(View.VISIBLE);
                } else {
                    rl_linkedin.setVisibility(View.GONE);
                    view_linkedin.setVisibility(View.GONE);
                }

            }
        });

        cb_twitter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rl_twitter.setVisibility(View.VISIBLE);
                    view_twitter.setVisibility(View.VISIBLE);
                } else {
                    rl_twitter.setVisibility(View.GONE);
                    view_twitter.setVisibility(View.GONE);
                }

            }
        });

        cb_others.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rl_others.setVisibility(View.VISIBLE);
                    view_others.setVisibility(View.VISIBLE);
                } else {
                    rl_others.setVisibility(View.GONE);
                    view_others.setVisibility(View.GONE);
                }

            }
        });
    }

    public void director2OnclickListener() {
        ll_your_personal_details2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout2.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout2.setVisibility(View.GONE);
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear2 != null) {
                        director_view_appear2.setVisibility(View.GONE);
                        iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    your_personal_details_layout2.setVisibility(View.VISIBLE);
                    director_view_appear2 = your_personal_details_layout2;
                    iv_director_visibility2 = iv_Visibility_your_personal_details2;
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_annual_income_details2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (annual_income_details_layout2.getVisibility() == View.VISIBLE) {
                    annual_income_details_layout2.setVisibility(View.GONE);
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear2 != null) {
                        director_view_appear2.setVisibility(View.GONE);
                        iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    annual_income_details_layout2.setVisibility(View.VISIBLE);
                    director_view_appear2 = annual_income_details_layout2;
                    iv_director_visibility2 = iv_Visibility_annual_income_details2;
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_social_details2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (social_details_layout2.getVisibility() == View.VISIBLE) {
                    social_details_layout2.setVisibility(View.GONE);
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear2 != null) {
                        director_view_appear2.setVisibility(View.GONE);
                        iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    social_details_layout2.setVisibility(View.VISIBLE);
                    director_view_appear2 = social_details_layout2;
                    iv_director_visibility2 = iv_Visibility_social_details2;
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_my_family_details2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_family_details_layout2.getVisibility() == View.VISIBLE) {
                    my_family_details_layout2.setVisibility(View.GONE);
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear2 != null) {
                        director_view_appear2.setVisibility(View.GONE);
                        iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    my_family_details_layout2.setVisibility(View.VISIBLE);
                    director_view_appear2 = my_family_details_layout2;
                    iv_director_visibility2 = iv_Visibility_my_family_details2;
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_personal_address2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (personal_address_layout2.getVisibility() == View.VISIBLE) {
                    personal_address_layout2.setVisibility(View.GONE);
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear2 != null) {
                        director_view_appear2.setVisibility(View.GONE);
                        iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    personal_address_layout2.setVisibility(View.VISIBLE);
                    director_view_appear2 = personal_address_layout2;
                    iv_director_visibility2 = iv_Visibility_personal_address2;
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_education_details2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (education_details_layout2.getVisibility() == View.VISIBLE) {
                    education_details_layout2.setVisibility(View.GONE);
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear2 != null) {
                        director_view_appear2.setVisibility(View.GONE);
                        iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    education_details_layout2.setVisibility(View.VISIBLE);
                    director_view_appear2 = education_details_layout2;
                    iv_director_visibility2 = iv_Visibility_education_details2;
                    iv_director_visibility2.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
    }

    public void director3OnclickListner() {
        ll_your_personal_details3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout3.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout3.setVisibility(View.GONE);
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear3 != null) {
                        director_view_appear3.setVisibility(View.GONE);
                        iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    your_personal_details_layout3.setVisibility(View.VISIBLE);
                    director_view_appear3 = your_personal_details_layout3;
                    iv_director_visibility3 = iv_Visibility_your_personal_details3;
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_annual_income_details3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (annual_income_details_layout3.getVisibility() == View.VISIBLE) {
                    annual_income_details_layout3.setVisibility(View.GONE);
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear3 != null) {
                        director_view_appear3.setVisibility(View.GONE);
                        iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    annual_income_details_layout3.setVisibility(View.VISIBLE);
                    director_view_appear3 = annual_income_details_layout3;
                    iv_director_visibility3 = iv_Visibility_annual_income_details3;
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_social_details3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (social_details_layout3.getVisibility() == View.VISIBLE) {
                    social_details_layout3.setVisibility(View.GONE);
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear3 != null) {
                        director_view_appear3.setVisibility(View.GONE);
                        iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    social_details_layout3.setVisibility(View.VISIBLE);
                    director_view_appear3 = social_details_layout3;
                    iv_director_visibility3 = iv_Visibility_social_details3;
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_my_family_details3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_family_details_layout3.getVisibility() == View.VISIBLE) {
                    my_family_details_layout3.setVisibility(View.GONE);
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear3 != null) {
                        director_view_appear3.setVisibility(View.GONE);
                        iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    my_family_details_layout3.setVisibility(View.VISIBLE);
                    director_view_appear3 = my_family_details_layout3;
                    iv_director_visibility3 = iv_Visibility_my_family_details3;
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_personal_address3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (personal_address_layout3.getVisibility() == View.VISIBLE) {
                    personal_address_layout3.setVisibility(View.GONE);
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear3 != null) {
                        director_view_appear3.setVisibility(View.GONE);
                        iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    personal_address_layout3.setVisibility(View.VISIBLE);
                    director_view_appear3 = personal_address_layout3;
                    iv_director_visibility3 = iv_Visibility_personal_address3;
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_education_details3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (education_details_layout3.getVisibility() == View.VISIBLE) {
                    education_details_layout3.setVisibility(View.GONE);
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear3 != null) {
                        director_view_appear3.setVisibility(View.GONE);
                        iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    education_details_layout3.setVisibility(View.VISIBLE);
                    director_view_appear3 = education_details_layout3;
                    iv_director_visibility3 = iv_Visibility_education_details3;
                    iv_director_visibility3.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
    }

    public void director4OnclickListner() {
        ll_your_personal_details4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout4.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout4.setVisibility(View.GONE);
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear4 != null) {
                        director_view_appear4.setVisibility(View.GONE);
                        iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    your_personal_details_layout4.setVisibility(View.VISIBLE);
                    director_view_appear4 = your_personal_details_layout4;
                    iv_director_visibility4 = iv_Visibility_your_personal_details4;
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_annual_income_details4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (annual_income_details_layout4.getVisibility() == View.VISIBLE) {
                    annual_income_details_layout4.setVisibility(View.GONE);
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear4 != null) {
                        director_view_appear4.setVisibility(View.GONE);
                        iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    annual_income_details_layout4.setVisibility(View.VISIBLE);
                    director_view_appear4 = annual_income_details_layout4;
                    iv_director_visibility4 = iv_Visibility_annual_income_details4;
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_social_details4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (social_details_layout4.getVisibility() == View.VISIBLE) {
                    social_details_layout4.setVisibility(View.GONE);
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear4 != null) {
                        director_view_appear4.setVisibility(View.GONE);
                        iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    social_details_layout4.setVisibility(View.VISIBLE);
                    director_view_appear4 = social_details_layout4;
                    iv_director_visibility4 = iv_Visibility_social_details4;
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_my_family_details4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_family_details_layout4.getVisibility() == View.VISIBLE) {
                    my_family_details_layout4.setVisibility(View.GONE);
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear4 != null) {
                        director_view_appear4.setVisibility(View.GONE);
                        iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    my_family_details_layout4.setVisibility(View.VISIBLE);
                    director_view_appear4 = my_family_details_layout4;
                    iv_director_visibility4 = iv_Visibility_my_family_details4;
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_personal_address4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (personal_address_layout4.getVisibility() == View.VISIBLE) {
                    personal_address_layout4.setVisibility(View.GONE);
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear4 != null) {
                        director_view_appear4.setVisibility(View.GONE);
                        iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    personal_address_layout4.setVisibility(View.VISIBLE);
                    director_view_appear4 = personal_address_layout4;
                    iv_director_visibility4 = iv_Visibility_personal_address4;
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_education_details4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (education_details_layout4.getVisibility() == View.VISIBLE) {
                    education_details_layout4.setVisibility(View.GONE);
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear4 != null) {
                        director_view_appear4.setVisibility(View.GONE);
                        iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    education_details_layout4.setVisibility(View.VISIBLE);
                    director_view_appear4 = education_details_layout4;
                    iv_director_visibility4 = iv_Visibility_education_details4;
                    iv_director_visibility4.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
    }

    public void setDirector1PersonalDetails() {

        Constants.YOUR_PERSONAL_DETAILS.FullName = et_full_name.getText().toString();
        Constants.YOUR_PERSONAL_DETAILS.Dob = et_dob.getText().toString();
        Constants.YOUR_PERSONAL_DETAILS.Email = et_email_id.getText().toString();
        Constants.YOUR_PERSONAL_DETAILS.MobileNumber = et_mobile_no.getText().toString();
        Constants.DIRECTOR.setYourPersonalDetails(Constants.YOUR_PERSONAL_DETAILS);

        Constants.SOCIAL_DETAILS.FaceBook = et_facebook.getText().toString();
        Constants.SOCIAL_DETAILS.LinkedIn = et_linkedin.getText().toString();
        Constants.SOCIAL_DETAILS.Twitter = et_twitter.getText().toString();
        Constants.SOCIAL_DETAILS.Others = et_others.getText().toString();
        Constants.DIRECTOR.setSocialDetails(Constants.SOCIAL_DETAILS);

        Constants.PERSONAL_ADDRESS.BusinessAddressPinCode = et_residential_address_pin_code.getText().toString();
        Constants.PERSONAL_ADDRESS.BusinessAddressHouseNo = et_residential_address_house_no_and_locality.getText().toString();
        Constants.PERSONAL_ADDRESS.BusinessAddressCity = et_residential_address_city.getText().toString();
        Constants.PERSONAL_ADDRESS.BusinessResidentialPremises = et_residential_address_state.getText().toString();

        Constants.PERSONAL_ADDRESS.PersonalAddressPinCode = et_permanent_address_pin_code.getText().toString();
        Constants.PERSONAL_ADDRESS.PersonalAddressHouseNo = et_pernanent_address_house_no_and_locality.getText().toString();
        Constants.PERSONAL_ADDRESS.PersonalAddressCity = et_permanent_address_city.getText().toString();
        Constants.PERSONAL_ADDRESS.PersonalAddressState = et_permanent_address_state.getText().toString();

        Constants.DIRECTOR.setPersonalAddress(Constants.PERSONAL_ADDRESS);

        Gson gson = new Gson();
        String json = gson.toJson(Constants.DIRECTOR);
        Log.e("json", "" + json);
    }

    public void setDirector1AnnualIncome(String income) {
        Constants.ANNUAL_INCOME_DETAILS.IncomeAmount = income;
        Constants.ANNUAL_INCOME_DETAILS.PanCardAddress = "";
        Constants.DIRECTOR.setAnnualIncomeDetails(Constants.ANNUAL_INCOME_DETAILS);
        Gson gson = new Gson();
        String json = gson.toJson(Constants.DIRECTOR);
        Log.e("json", "" + json);
    }

    public void setDirector1PermanentAddressPremises(String residentialPremises) {
        Constants.PERSONAL_ADDRESS.PersonalResidentialPremises = residentialPremises;
       // Constants.ANNUAL_INCOME_DETAILS.PanCardAddress = "";
        Constants.DIRECTOR.setAnnualIncomeDetails(Constants.ANNUAL_INCOME_DETAILS);
        Gson gson = new Gson();
        String json = gson.toJson(Constants.DIRECTOR);
        Log.e("json", "" + json);
    }

    public void director1PersonalDetailsChangeListener() {
        et_full_name.addTextChangedListener(director1PersonalAddressChangeListener);
        et_email_id.addTextChangedListener(director1PersonalAddressChangeListener);
        et_mobile_no.addTextChangedListener(director1PersonalAddressChangeListener);

        et_facebook.addTextChangedListener(director1PersonalAddressChangeListener);
        et_linkedin.addTextChangedListener(director1PersonalAddressChangeListener);
        et_twitter.addTextChangedListener(director1PersonalAddressChangeListener);
        et_others.addTextChangedListener(director1PersonalAddressChangeListener);

        et_residential_address_pin_code.addTextChangedListener(director1PersonalAddressChangeListener);
        et_residential_address_house_no_and_locality.addTextChangedListener(director1PersonalAddressChangeListener);
        et_residential_address_city.addTextChangedListener(director1PersonalAddressChangeListener);
        et_residential_address_state.addTextChangedListener(director1PersonalAddressChangeListener);

        et_permanent_address_pin_code.addTextChangedListener(director1PersonalAddressChangeListener);
        et_pernanent_address_house_no_and_locality.addTextChangedListener(director1PersonalAddressChangeListener);
        et_permanent_address_city.addTextChangedListener(director1PersonalAddressChangeListener);
        et_permanent_address_state.addTextChangedListener(director1PersonalAddressChangeListener);

        rg_permanent_address_residential_premises.setOnCheckedChangeListener(director1PermanentAddressPremises);

        rg_annual_income.setOnCheckedChangeListener(director1IncomeChangeListener);
    }

    public RadioGroup.OnCheckedChangeListener director1IncomeChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            Log.e("annual income", "annual income" + rb.getText());
            setDirector1AnnualIncome(rb.getText().toString());
        }
    };

    public RadioGroup.OnCheckedChangeListener director1PermanentAddressPremises = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            Log.e("annual income", "annual income" + rb.getText());
            setDirector1AnnualIncome(rb.getText().toString());
        }
    };

    public TextWatcher director1PersonalAddressChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            setDirector1PersonalDetails();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

}
