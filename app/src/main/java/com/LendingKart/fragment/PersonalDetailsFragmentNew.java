package com.LendingKart.fragment;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.LendingKart.R;
import com.LendingKart.customviews.RobotoLightTextView;
import com.LendingKart.customviews.TresbuEditText;

/**
 * Created by chakrabo on 9/22/2015.
 */
public class PersonalDetailsFragmentNew extends Fragment implements View.OnClickListener {
    static final int MIN_DISTANCE = 50;
    View mRootView;
    LinearLayout ll_personal_details_container;
    RobotoLightTextView tv_add_more;
    ImageView iv_personals_visibility, iv_director_visibility,iv_addMore;
    RelativeLayout view_appear = null, director_view_appear = null;
    int i = 0;
    private float x1, x2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mRootView = inflater.inflate(R.layout.personaldetailsmainlayout, container, false);
        initView(mRootView);
        tv_add_more.performClick();
        return mRootView;
    }

    public void initView(View mRootView) {
        ll_personal_details_container = (LinearLayout) mRootView.findViewById(R.id.ll_personal_details_container);
        tv_add_more = (RobotoLightTextView) mRootView.findViewById(R.id.tv_add_more);
        iv_addMore=(ImageView)mRootView.findViewById(R.id.iv_addMore);

        tv_add_more.setOnClickListener(this);
        iv_addMore.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_add_more:
                ll_personal_details_container.addView(getUploadView("", i));
                i++;
                break;
            case R.id.iv_addMore:
                ll_personal_details_container.addView(getUploadView("", i));
                i++;
                break;

        }
    }

    private View getUploadView(final String path, int id) {
        String[] file = path.split("/");
        String file_name = file[file.length - 1];
        LayoutInflater layoutInflater =
                (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mChildView = layoutInflater.inflate(R.layout.activity_personals, null);

        final LinearLayout ll_director, ll_your_personal_details, ll_annual_income_details, ll_social_details, ll_my_family_details,
                ll_personal_address, ll_education_details;

        final RelativeLayout director_layout, your_personal_details_layout, annual_income_details_layout, social_details_layout,
                my_family_details_layout, personal_address_layout, education_details_layout, residential_address_layout, permanent_address_layout;

        final RelativeLayout rl_director;

        final ToggleButton tb_residential_address, tb_permanent_address;

        final CheckBox cb_facebook, cb_linkedin, cb_twitter, cb_others;

        /** STATUS IMAGES ***/
        final ImageView iv_Status_director, iv_Status_your_personal_details, iv_Status_annual_income_details, iv_Status_personal_address,
                iv_Status_education_details, iv_Status_my_family_details, iv_Status_social_details;


        /*****************************
         * @@@@@@@@ VISIBILITY IMAGES
         ****************************/
        final ImageView iv_Visibility_director, iv_Visibility_your_personal_details, iv_Visibility_annual_income_details,
                iv_Visibility_personal_address, iv_Visibility_education_details, iv_Visibility_my_family_details, iv_Visibility_social_details;


        /*************
         * @@@@@@@@@@ SOCIAL IMAGE HINT VIEWS @@@@@@@@@
         ********/
        final ImageView iv_facebook;

        /**** RELATIVE LAYOUTS FOR SOCIAL  *****/
        final RelativeLayout rl_facebook, rl_linkedin, rl_twitter, rl_others;
        final View view_facebook, view_linkedin, view_twitter, view_others;

        //Your personal details
        final TresbuEditText et_full_name, et_mobile_no, et_email_id, et_dob;

        rl_director = (RelativeLayout) mChildView.findViewById(R.id.rl_director);

        ll_director = (LinearLayout) mChildView.findViewById(R.id.ll_director);
        ll_director.setId(id);
        ll_your_personal_details = (LinearLayout) mChildView.findViewById(R.id.ll_your_personal_details);
        ll_annual_income_details = (LinearLayout) mChildView.findViewById(R.id.ll_annual_income_details);
        ll_social_details = (LinearLayout) mChildView.findViewById(R.id.ll_social_details);
        ll_my_family_details = (LinearLayout) mChildView.findViewById(R.id.ll_my_family_details);
        ll_personal_address = (LinearLayout) mChildView.findViewById(R.id.ll_personal_address);
        ll_education_details = (LinearLayout) mChildView.findViewById(R.id.ll_education_details);

        director_layout = (RelativeLayout) mChildView.findViewById(R.id.director_layout);
        your_personal_details_layout = (RelativeLayout) mChildView.findViewById(R.id.your_personal_details_layout);
        annual_income_details_layout = (RelativeLayout) mChildView.findViewById(R.id.annual_income_details_layout);
        social_details_layout = (RelativeLayout) mChildView.findViewById(R.id.social_details_layout);
        my_family_details_layout = (RelativeLayout) mChildView.findViewById(R.id.my_family_details_layout);
        personal_address_layout = (RelativeLayout) mChildView.findViewById(R.id.personal_address_layout);
        education_details_layout = (RelativeLayout) mChildView.findViewById(R.id.education_details_layout);
        residential_address_layout = (RelativeLayout) mChildView.findViewById(R.id.residential_address_layout);
        permanent_address_layout = (RelativeLayout) mChildView.findViewById(R.id.permanent_address_layout);

        tb_residential_address = (ToggleButton) mChildView.findViewById(R.id.tb_residential_address);
        tb_permanent_address = (ToggleButton) mChildView.findViewById(R.id.tb_permanent_address);


        /**** @@@@@@@@@@@@@@ STATUS VIEW @@@@@@@@@ ***********/
        iv_Status_director = (ImageView) mChildView.findViewById(R.id.iv_Status_director);
        iv_Status_your_personal_details = (ImageView) mChildView.findViewById(R.id.iv_Status_your_personal_details);
        iv_Status_annual_income_details = (ImageView) mChildView.findViewById(R.id.iv_Status_annual_income_details);
        iv_Status_personal_address = (ImageView) mChildView.findViewById(R.id.iv_Status_personal_address);
        iv_Status_education_details = (ImageView) mChildView.findViewById(R.id.iv_Status_education_details);
        iv_Status_my_family_details = (ImageView) mChildView.findViewById(R.id.iv_Status_my_family_details);
        iv_Status_social_details = (ImageView) mChildView.findViewById(R.id.iv_Status_social_details);

        /***********  @@@@@@@@@@ VISIBILITY IMAGES @@@@@@@@@ ************/
        iv_Visibility_director = (ImageView) mChildView.findViewById(R.id.iv_Visibility_director);
        iv_Visibility_your_personal_details = (ImageView) mChildView.findViewById(R.id.iv_Visibility_your_personal_details);
        iv_Visibility_annual_income_details = (ImageView) mChildView.findViewById(R.id.iv_Visibility_annual_income_details);
        iv_Visibility_personal_address = (ImageView) mChildView.findViewById(R.id.iv_Visibility_personal_address);
        iv_Visibility_education_details = (ImageView) mChildView.findViewById(R.id.iv_Visibility_education_details);
        iv_Visibility_my_family_details = (ImageView) mChildView.findViewById(R.id.iv_Visibility_my_family_details);
        iv_Visibility_social_details = (ImageView) mChildView.findViewById(R.id.iv_Visibility_social_details);

        /************@@@@@@@@@@@@@ SOCIAL TEXT VIEWS  @@@@@@ *******/
        cb_facebook = (CheckBox) mChildView.findViewById(R.id.cb_facebook);
        cb_linkedin = (CheckBox) mChildView.findViewById(R.id.cb_linkedin);
        cb_twitter = (CheckBox) mChildView.findViewById(R.id.cb_twitter);
        cb_others = (CheckBox) mChildView.findViewById(R.id.cb_others);

        rl_facebook = (RelativeLayout) mChildView.findViewById(R.id.rl_facebook);
        rl_linkedin = (RelativeLayout) mChildView.findViewById(R.id.rl_linkedin);
        rl_twitter = (RelativeLayout) mChildView.findViewById(R.id.rl_twitter);
        rl_others = (RelativeLayout) mChildView.findViewById(R.id.rl_others);

        view_facebook = (View) mChildView.findViewById(R.id.view_facebook);
        view_linkedin = (View) mChildView.findViewById(R.id.view_linkedin);
        view_twitter = (View) mChildView.findViewById(R.id.view_twitter);
        view_others = (View) mChildView.findViewById(R.id.view_others);

        RobotoLightTextView tv_parent_group_text = (RobotoLightTextView) mChildView.findViewById(R.id.tv_parent_group_text);

        //your personal details
        et_full_name = (TresbuEditText) mChildView.findViewById(R.id.et_full_name);
        et_mobile_no = (TresbuEditText) mChildView.findViewById(R.id.et_mobile_no);
        et_email_id = (TresbuEditText) mChildView.findViewById(R.id.et_email_id);
        et_dob = (TresbuEditText) mChildView.findViewById(R.id.et_dob);

        tv_parent_group_text.setText("DIRECTOR" + (i + 1));
        iv_personals_visibility = iv_Visibility_director;
        iv_director_visibility = iv_Visibility_your_personal_details;
        ll_director.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (director_layout.getVisibility() == View.VISIBLE) {
                    director_layout.setVisibility(View.GONE);
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (view_appear != null) {
                        view_appear.setVisibility(View.GONE);
                        iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    director_layout.setVisibility(View.VISIBLE);
                    view_appear = director_layout;
                    iv_personals_visibility = iv_Visibility_director;
                    iv_personals_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        //DIRECTOR VIEW CLICK
        ll_your_personal_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    your_personal_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = your_personal_details_layout;
                    iv_director_visibility = iv_Visibility_your_personal_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_annual_income_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));}
                if (your_personal_details_layout.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                }
                if (annual_income_details_layout.getVisibility() == View.VISIBLE) {
                    annual_income_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    annual_income_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = annual_income_details_layout;
                    iv_director_visibility = iv_Visibility_annual_income_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_social_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));}
                if (social_details_layout.getVisibility() == View.VISIBLE) {
                    social_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    social_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = social_details_layout;
                    iv_director_visibility = iv_Visibility_social_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_my_family_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));}
                if (my_family_details_layout.getVisibility() == View.VISIBLE) {
                    my_family_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    my_family_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = my_family_details_layout;
                    iv_director_visibility = iv_Visibility_my_family_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_personal_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));}
                if (personal_address_layout.getVisibility() == View.VISIBLE) {
                    personal_address_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    personal_address_layout.setVisibility(View.VISIBLE);
                    director_view_appear = personal_address_layout;
                    iv_director_visibility = iv_Visibility_personal_address;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_education_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (your_personal_details_layout.getVisibility() == View.VISIBLE) {
                    your_personal_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));}
                if (education_details_layout.getVisibility() == View.VISIBLE) {
                    education_details_layout.setVisibility(View.GONE);
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (director_view_appear != null) {
                        director_view_appear.setVisibility(View.GONE);
                        iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    education_details_layout.setVisibility(View.VISIBLE);
                    director_view_appear = education_details_layout;
                    iv_director_visibility = iv_Visibility_education_details;
                    iv_director_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        tb_residential_address.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    residential_address_layout.setVisibility(View.GONE);
                } else {
                    residential_address_layout.setVisibility(View.VISIBLE);
                }

            }
        });
        tb_permanent_address.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    permanent_address_layout.setVisibility(View.GONE);
                } else {
                    permanent_address_layout.setVisibility(View.VISIBLE);
                }

            }
        });

        cb_facebook.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rl_facebook.setVisibility(View.VISIBLE);
                    view_facebook.setVisibility(View.VISIBLE);
                } else {
                    rl_facebook.setVisibility(View.GONE);
                    view_facebook.setVisibility(View.GONE);
                }

            }
        });

        cb_linkedin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rl_linkedin.setVisibility(View.VISIBLE);
                    view_linkedin.setVisibility(View.VISIBLE);
                } else {
                    rl_linkedin.setVisibility(View.GONE);
                    view_linkedin.setVisibility(View.GONE);
                }

            }
        });

        cb_twitter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rl_twitter.setVisibility(View.VISIBLE);
                    view_twitter.setVisibility(View.VISIBLE);
                } else {
                    rl_twitter.setVisibility(View.GONE);
                    view_twitter.setVisibility(View.GONE);
                }

            }
        });

        cb_others.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rl_others.setVisibility(View.VISIBLE);
                    view_others.setVisibility(View.VISIBLE);
                } else {
                    rl_others.setVisibility(View.GONE);
                    view_others.setVisibility(View.GONE);
                }

            }
        });

        tv_parent_group_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        float deltaX = x2 - x1;
                        if (Math.abs(deltaX) > MIN_DISTANCE) {
                            ((LinearLayout) mChildView.getParent()).removeView(mChildView);
                        } else {
                            // consider as something else - a screen tap for example
                        }
                        break;
                }
                return true;
            }
        });

        //your personal details
        et_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        //DO SOMETHING
                        et_dob.setText("" + dayOfMonth + "/" + monthOfYear + "/" + year);
                    }
                }, 2015, 02, 24).show();

            }
        });

        return mChildView;
    }

}
