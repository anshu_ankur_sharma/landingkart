package com.LendingKart.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.LendingKart.R;
import com.LendingKart.customviews.TresbuEditText;
import com.LendingKart.customviews.TresbuRadioButton;

/**
 * Created by sharmaan on 12-09-2015.
 */
public class BusinessDetailFragment extends Fragment {

    View mRootView = null;

    RelativeLayout rlBusinessDetail, rlCompanyRegisteredAs, rlTurnOver, rlBusinessAddres, rlBankWith, rlBusinessTillnow, rlTaxation;

    ImageView ivBusinessContactDetail, ivCompanyRegistered, ivBusinessNature, ivProductsSell, ivBusinessTurnOver, ivBusinessTillNow,
            ivBusinessAddress, ivSellingON, ivBankWIth, ivTaxation;

    ImageView ivStatusVisibiltyBusinessContactDetail, ivStatusVisibiltyCompanyRegistered, ivStatusVisibiltyBusinessNature, ivStatusVisibiltyProductsSell, ivStatusVisibiltyBusinessTurnOver, ivStatusVisibiltyBusinessTillNow,
            ivStatusVisibiltyBusinessAddress, ivStatusVisibiltySellingON, ivStatusVisibiltyBankWIth, ivStatusVisibiltyTaxation;

    RelativeLayout llBusinessContactDetail, llCompanyRegistered, llBusinessNature, llProductsSell, llBusinessTurnOver, llBusinessTillNow,
            llBusinessAddress, llSellingON, llBankWIth, llTaxation;

    CheckBox chOtherBank;
    TresbuEditText etCompanyName, etPhoneNumber, etEmailId, etNumberOfEmployee, tvBankPleaseVerify;
    TresbuRadioButton tvProprietorship, tvPvtLtd, tvPartnership, tvLLP, tvOnePersonCompany, tvLimitedCompany;
    TresbuRadioButton tvProprietorshipSelf, tvProprietorshipParent, tvProprietorshipSpouse, tvProprietorshipRelative,
            tvPvtLtdOne, tvPvtLtdTwo, tvPvtLtdThree, tvPvtLtdMoreThanFour, tvPartnershipOne, tvPartnershipTwo, tvPartnershipThree, tvPartnershipMoreThanFour,
            tvLLPOne, tvLLPTwo, tvLLPThree, tvLLPMoreThanFour, tvLimitedCompanyOne, tvLimitedCompanyTwo, tvLimitedCompanyThree, tvLimitedCompanyMoreThanFour;
    LinearLayout llProprietorship, llPvtLtd, llPartnership, llLLP, llLimitedCompany;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater
                .inflate(R.layout.businessdetailfragment, container, false);

        initialiseIds(mRootView);

        llBusinessContactDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlBusinessDetail.getVisibility() == View.VISIBLE) {
                    rlBusinessDetail.setVisibility(View.GONE);
                    ivStatusVisibiltyBusinessContactDetail.setImageResource(R.drawable.expand);
                } else {
                    ivStatusVisibiltyBusinessContactDetail.setImageResource(R.drawable.collapse);
                    rlBusinessDetail.setVisibility(View.VISIBLE);
                }
            }
        });

        ivStatusVisibiltyBusinessContactDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlBusinessDetail.getVisibility() == View.VISIBLE) {
                    rlBusinessDetail.setVisibility(View.GONE);
                    ivStatusVisibiltyBusinessContactDetail.setImageResource(R.drawable.expand);
                } else {
                    ivStatusVisibiltyBusinessContactDetail.setImageResource(R.drawable.collapse);
                    rlBusinessDetail.setVisibility(View.VISIBLE);
                }
            }
        });


        llCompanyRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlCompanyRegisteredAs.getVisibility() == View.VISIBLE) {
                    rlCompanyRegisteredAs.setVisibility(View.GONE);
                    ivStatusVisibiltyCompanyRegistered.setImageResource(R.drawable.expand);
                } else {
                    ivStatusVisibiltyCompanyRegistered.setImageResource(R.drawable.collapse);
                    rlCompanyRegisteredAs.setVisibility(View.VISIBLE);
                }
            }
        });

        ivStatusVisibiltyCompanyRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlCompanyRegisteredAs.getVisibility() == View.VISIBLE) {
                    rlCompanyRegisteredAs.setVisibility(View.GONE);
                    ivStatusVisibiltyCompanyRegistered.setImageResource(R.drawable.expand);
                } else {
                    rlCompanyRegisteredAs.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyCompanyRegistered.setImageResource(R.drawable.collapse);
                }
            }
        });

        llBusinessTurnOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlTurnOver.getVisibility() == View.VISIBLE) {
                    rlTurnOver.setVisibility(View.GONE);
                    ivStatusVisibiltyBusinessTurnOver.setImageResource(R.drawable.expand);
                } else {
                    rlTurnOver.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyBusinessTurnOver.setImageResource(R.drawable.collapse);
                }
            }
        });

        ivBusinessTurnOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlTurnOver.getVisibility() == View.VISIBLE) {
                    rlTurnOver.setVisibility(View.GONE);
                    ivStatusVisibiltyBusinessTurnOver.setImageResource(R.drawable.expand);
                } else {
                    rlTurnOver.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyBusinessTurnOver.setImageResource(R.drawable.collapse);
                }
            }
        });

        llTaxation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlTaxation.getVisibility() == View.VISIBLE) {
                    rlTaxation.setVisibility(View.GONE);
                    ivStatusVisibiltyTaxation.setImageResource(R.drawable.expand);
                } else {
                    rlTaxation.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyTaxation.setImageResource(R.drawable.collapse);
                }
            }
        });

        ivTaxation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlTaxation.getVisibility() == View.VISIBLE) {
                    rlTaxation.setVisibility(View.GONE);
                    ivStatusVisibiltyTaxation.setImageResource(R.drawable.expand);
                } else {
                    rlTaxation.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyTaxation.setImageResource(R.drawable.collapse);
                }
            }
        });

        llBankWIth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlBankWith.getVisibility() == View.VISIBLE) {
                    rlBankWith.setVisibility(View.GONE);
                    ivStatusVisibiltyBankWIth.setImageResource(R.drawable.expand);
                } else {
                    rlBankWith.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyBankWIth.setImageResource(R.drawable.collapse);
                }
            }
        });

        ivBankWIth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlBankWith.getVisibility() == View.VISIBLE) {
                    rlBankWith.setVisibility(View.GONE);
                    ivStatusVisibiltyBankWIth.setImageResource(R.drawable.expand);
                } else {
                    rlBankWith.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyBankWIth.setImageResource(R.drawable.collapse);
                }
            }
        });

        llBusinessAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlBusinessAddres.getVisibility() == View.VISIBLE) {
                    rlBusinessAddres.setVisibility(View.GONE);
                    ivStatusVisibiltyBusinessAddress.setImageResource(R.drawable.expand);
                } else {
                    rlBusinessAddres.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyBusinessAddress.setImageResource(R.drawable.collapse);
                }
            }
        });

        ivBusinessAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlBusinessAddres.getVisibility() == View.VISIBLE) {
                    rlBusinessAddres.setVisibility(View.GONE);
                    ivStatusVisibiltyBusinessAddress.setImageResource(R.drawable.expand);
                } else {
                    rlBusinessAddres.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyBusinessAddress.setImageResource(R.drawable.collapse);
                }
            }
        });

        llBusinessTillNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlBusinessTillnow.getVisibility() == View.VISIBLE) {
                    rlBusinessTillnow.setVisibility(View.GONE);
                    ivStatusVisibiltyBusinessAddress.setImageResource(R.drawable.expand);
                } else {
                    rlBusinessTillnow.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyBusinessAddress.setImageResource(R.drawable.collapse);
                }
            }
        });

        ivBusinessTillNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlBusinessTillnow.getVisibility() == View.VISIBLE) {
                    rlBusinessTillnow.setVisibility(View.GONE);
                    ivStatusVisibiltyBusinessTillNow.setImageResource(R.drawable.expand);
                } else {
                    rlBusinessTillnow.setVisibility(View.VISIBLE);
                    ivStatusVisibiltyBusinessTillNow.setImageResource(R.drawable.collapse);
                }
            }
        });

        tvProprietorship.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvProprietorship.isChecked()) {
                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.VISIBLE);
                } else {
                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.GONE);
                }
            }
        });

        tvPvtLtd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvPvtLtd.isChecked()) {
                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.VISIBLE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.GONE);
                } else {
                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.GONE);
                }
            }
        });

        tvOnePersonCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.GONE);

            }
        });

        tvPartnership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvPartnership.isChecked()) {
                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.VISIBLE);
                    llProprietorship.setVisibility(View.GONE);
                } else {
                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.GONE);
                }
            }
        });

        tvLLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvLLP.isChecked()) {
                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.VISIBLE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.GONE);
                } else {
                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.GONE);

                }
            }
        });


        tvLimitedCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvLimitedCompany.isChecked()) {
                    llLimitedCompany.setVisibility(View.VISIBLE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.GONE);
                } else {
                    llLimitedCompany.setVisibility(View.GONE);
                    llLLP.setVisibility(View.GONE);
                    llPvtLtd.setVisibility(View.GONE);
                    llPartnership.setVisibility(View.GONE);
                    llProprietorship.setVisibility(View.GONE);
                }
            }
        });

        chOtherBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chOtherBank.isChecked())
                {
                    tvBankPleaseVerify.setVisibility(View.VISIBLE);
                }else{
                    tvBankPleaseVerify.setVisibility(View.GONE);
                }
            }
        });

        return mRootView;
    }

    private void initialiseIds(View mRootView) {

        llBusinessContactDetail = (RelativeLayout) mRootView.findViewById(R.id.rlBusinessContactDetail);
        llCompanyRegistered = (RelativeLayout) mRootView.findViewById(R.id.rlCompanyRegistered);
        llBusinessNature = (RelativeLayout) mRootView.findViewById(R.id.rlBusinessNature);
        llProductsSell = (RelativeLayout) mRootView.findViewById(R.id.rlProductsSell);
        llBusinessTurnOver = (RelativeLayout) mRootView.findViewById(R.id.rlBusinessTurnOver);
        llBusinessTillNow = (RelativeLayout) mRootView.findViewById(R.id.rlBusinessTillNow);
        llBusinessAddress = (RelativeLayout) mRootView.findViewById(R.id.rlBusinessAddress);
        llSellingON = (RelativeLayout) mRootView.findViewById(R.id.rlSellingON);
        llBankWIth = (RelativeLayout) mRootView.findViewById(R.id.rlBankWIth);
        llTaxation = (RelativeLayout) mRootView.findViewById(R.id.rlTaxationView);


        ivBusinessContactDetail = (ImageView) mRootView.findViewById(R.id.ivStatusbusinessContactDetail);
        ivCompanyRegistered = (ImageView) mRootView.findViewById(R.id.ivStatusCompanyRegistered);
        ivBusinessNature = (ImageView) mRootView.findViewById(R.id.ivStatusBusinessNature);
        ivProductsSell = (ImageView) mRootView.findViewById(R.id.ivStatusProductsSell);
        ivBusinessTurnOver = (ImageView) mRootView.findViewById(R.id.ivStatusBusinessTurnOver);
        ivBusinessTillNow = (ImageView) mRootView.findViewById(R.id.ivStatusBusinessTillNow);
        ivBusinessAddress = (ImageView) mRootView.findViewById(R.id.ivStatusBusinessAddress);
        ivSellingON = (ImageView) mRootView.findViewById(R.id.ivStatusSellingON);
        ivBankWIth = (ImageView) mRootView.findViewById(R.id.ivStatusBankWIth);
        ivTaxation = (ImageView) mRootView.findViewById(R.id.ivStatusTaxation);


        ivStatusVisibiltyBusinessContactDetail = (ImageView) mRootView.findViewById(R.id.ivVisibiltyStatusbusinessContactDetail);
        ivStatusVisibiltyCompanyRegistered = (ImageView) mRootView.findViewById(R.id.ivVisibiltyStatusCompanyRegistered);
        ivStatusVisibiltyBusinessNature = (ImageView) mRootView.findViewById(R.id.ivVisibiltyStatusBusinessNature);
        ivStatusVisibiltyProductsSell = (ImageView) mRootView.findViewById(R.id.ivVisibiltyStatusProductsSell);
        ivStatusVisibiltyBusinessTurnOver = (ImageView) mRootView.findViewById(R.id.ivVisibiltyStatusBusinessTurnOver);
        ivStatusVisibiltyBusinessTillNow = (ImageView) mRootView.findViewById(R.id.ivVisibiltyStatusBusinessTillNow);
        ivStatusVisibiltyBusinessAddress = (ImageView) mRootView.findViewById(R.id.ivVisibiltyStatusBusinessAddress);
        ivStatusVisibiltySellingON = (ImageView) mRootView.findViewById(R.id.ivVisibiltySellingON);
        ivStatusVisibiltyBankWIth = (ImageView) mRootView.findViewById(R.id.ivVisibiltyStatusBankWIth);
        ivStatusVisibiltyTaxation = (ImageView) mRootView.findViewById(R.id.ivVisibiltyStatusTaxation);


        rlBusinessDetail = (RelativeLayout) mRootView.findViewById(R.id.businessLayout);
        rlCompanyRegisteredAs = (RelativeLayout) mRootView.findViewById(R.id.rlCompanyregistedAs);
        rlTurnOver = (RelativeLayout) mRootView.findViewById(R.id.rlTurnOver);
        rlBusinessAddres = (RelativeLayout) mRootView.findViewById(R.id.rlbusinesAddress);
        rlBankWith = (RelativeLayout) mRootView.findViewById(R.id.rlbankWith);
        rlBusinessTillnow = (RelativeLayout) mRootView.findViewById(R.id.rlbusinessTillNow);
        rlTaxation = (RelativeLayout) mRootView.findViewById(R.id.rlTaxation);

        etCompanyName = (TresbuEditText) mRootView.findViewById(R.id.etCompanyLabel);
        etPhoneNumber = (TresbuEditText) mRootView.findViewById(R.id.etContactLabel);
        etEmailId = (TresbuEditText) mRootView.findViewById(R.id.etEmailId);
        etNumberOfEmployee = (TresbuEditText) mRootView.findViewById(R.id.etNumberOfEmployee);
        tvBankPleaseVerify=(TresbuEditText)mRootView.findViewById(R.id.pleaseSpecify);

        tvProprietorship = (TresbuRadioButton) mRootView.findViewById(R.id.tvProprietorship);
        tvPvtLtd = (TresbuRadioButton) mRootView.findViewById(R.id.tvPvtLtd);
        tvPartnership = (TresbuRadioButton) mRootView.findViewById(R.id.tvPartnership);
        tvLLP = (TresbuRadioButton) mRootView.findViewById(R.id.tvLLP);
        tvOnePersonCompany = (TresbuRadioButton) mRootView.findViewById(R.id.tvOnePersonCompany);
        tvLimitedCompany = (TresbuRadioButton) mRootView.findViewById(R.id.tvLimitedCompany);

        tvProprietorshipSelf = (TresbuRadioButton) mRootView.findViewById(R.id.tvSelf);
        tvProprietorshipParent = (TresbuRadioButton) mRootView.findViewById(R.id.tvParent);
        tvProprietorshipSpouse = (TresbuRadioButton) mRootView.findViewById(R.id.tvSpouse);
        tvProprietorshipRelative = (TresbuRadioButton) mRootView.findViewById(R.id.tvRelative);
        tvPvtLtdOne = (TresbuRadioButton) mRootView.findViewById(R.id.tvPvtLtdOne);
        tvPvtLtdTwo = (TresbuRadioButton) mRootView.findViewById(R.id.tvPvtLtdTwo);
        tvPvtLtdThree = (TresbuRadioButton) mRootView.findViewById(R.id.tvPvtLtdThree);
        tvPvtLtdMoreThanFour = (TresbuRadioButton) mRootView.findViewById(R.id.tvPvtLtdMoreThanFour);
        tvPartnershipOne = (TresbuRadioButton) mRootView.findViewById(R.id.tvPartnerShipOne);
        tvPartnershipTwo = (TresbuRadioButton) mRootView.findViewById(R.id.tvPartnerShipTwo);
        tvPartnershipThree = (TresbuRadioButton) mRootView.findViewById(R.id.tvPartnerShipThree);
        tvPartnershipMoreThanFour = (TresbuRadioButton) mRootView.findViewById(R.id.tvPartnerShipMoreThanFour);
        tvLLPOne = (TresbuRadioButton) mRootView.findViewById(R.id.tvLLPOne);
        tvLLPTwo = (TresbuRadioButton) mRootView.findViewById(R.id.tvLLPTwo);
        tvLLPThree = (TresbuRadioButton) mRootView.findViewById(R.id.tvLLPThree);
        tvLLPMoreThanFour = (TresbuRadioButton) mRootView.findViewById(R.id.tvLLPMoreThanFour);
        tvLimitedCompanyOne = (TresbuRadioButton) mRootView.findViewById(R.id.tvLimitedCompanyOne);
        tvLimitedCompanyTwo = (TresbuRadioButton) mRootView.findViewById(R.id.tvLimitedCompanyTwo);
        tvLimitedCompanyThree = (TresbuRadioButton) mRootView.findViewById(R.id.tvLimitedCompanyThree);
        tvLimitedCompanyMoreThanFour = (TresbuRadioButton) mRootView.findViewById(R.id.tvLimitedCompanyMoreThanFour);

        llProprietorship = (LinearLayout) mRootView.findViewById(R.id.llProprietorship);
        llPvtLtd = (LinearLayout) mRootView.findViewById(R.id.llPvtLtd);
        llPartnership = (LinearLayout) mRootView.findViewById(R.id.llPartnerShip);
        llLLP = (LinearLayout) mRootView.findViewById(R.id.llLLP);
        llLimitedCompany = (LinearLayout) mRootView.findViewById(R.id.llLimitedCompany);

        chOtherBank=(CheckBox)mRootView.findViewById(R.id.otherBank);
    }

}
