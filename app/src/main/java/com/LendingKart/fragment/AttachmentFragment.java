package com.LendingKart.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.LendingKart.R;
import com.LendingKart.customviews.RobotoLightTextView;
import com.LendingKart.model.Attachments;
import com.LendingKart.util.Constants;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by sharmaan on 12-09-2015.
 */
public class AttachmentFragment extends Fragment {

    final int UPLOAD_REQ_CODE_VAT_RETURNS = 101, UPLOAD_REQ_BANK_STATEMENT_COMPANY = 102, UPLOAD_REQ_BANK_STATEMENT_DIRECTOR = 103,
            UPLOAD_REQ_PAN_CARD_PERSONAL = 104, UPLOAD_REQ_AADHAR_CARD_PERSONAL = 105, UPLOAD_REQ_DRIVING_LICENCE_PERSONAL = 106,
            UPLOAD_REQ_PASSPORT_PERSONAL = 107, UPLOAD_REQ_VOTER_ID_PERSONAL = 108, UPLOAD_REQ_INCOME_TAX_RETURNS_PERSONAL = 109,
            UPLOAD_REQ_PAN_CARD_COMPANY = 110, UPLOAD_REQ_INCOME_TAX_RETURNS_COMPANY = 111;
    private View mRootView;
    private RelativeLayout view_appear = null, important_document_view_appear = null, kyc_view_appear = null;
    private LinearLayout ll_Important_document, ll_kyc, ll_Vat_returns, ll_Bank_statement, ll_Bank_statement_director, ll_Pan_card_personal,
            ll_aadhar_card_personal, ll_driving_licence_personal, ll_passport_personal, ll_voter_id_personal, ll_income_tax_returns_personal,
            ll_pan_card_company, ll_income_tax_returns_company;
    private RelativeLayout important_document_layout, kyc_layout, vat_returns_layout, bank_statement_company_layout,
            bank_statement_director_layout, pan_card_personal_layout, aadhar_card_personal_layout, driving_licence_personal_layout,
            passport_personal_layout, voter_id_personal_layout, income_tax_returns_personal_layout, pan_card_company_layout,
            income_tax_returns_company_layout;
    private Button btn_vat_returns_upload, btn_bank_statement_company_upload, btn_bank_statement_director_upload,
            btn_pan_card_personal_upload, btn_aadhar_card_personal_upload, btn_driving_licence_personal_upload,
            btn_passport_personal_upload, btn_voter_id_personal_upload, btn_income_tax_returns_personal_upload,
            btn_pan_card_company_upload, btn_income_tax_returns_company_upload;
    private LinearLayout ll_Vat_returns_container, ll_bank_statement_company_container, ll_bank_statement_director_container,
            ll_pan_card_personal_container, ll_aadhar_card_personal_container, ll_driving_licence_personal_container,
            ll_passport_personal_container, ll_voter_id_personal_container, ll_income_tax_returns_personal_container,
            ll_pan_card_company_container, ll_income_tax_returns_company_container;
    /*****
     * @@@@@@@ STATUS IMAGES @@@@@@
     *******/
    private ImageView iv_Status_Important_document, iv_Status_kyc, iv_Status_Vat_returns, iv_Status_Bank_statement, iv_Status_Bank_statement_director,
            iv_Status_Pan_card_personal, iv_Status_aadhar_card_personal, iv_Status_driving_licence_personal, iv_Status_passport_personal,
            iv_Status_voter_id_personal, iv_Status_income_tax_returns_personal, iv_Status_pan_card_company, iv_Status_income_tax_returns_company;
    /******
     * @@@@@@ VISIBILITY IMAGES @@@@@
     ******/
    private ImageView iv_Visibility_Important_document, iv_Visibility_kyc, iv_Visibility_Vat_returns, iv_Visibility_Bank_statement, iv_Visibility_Bank_statement_director,
            iv_Visibility_Pan_card_personal, iv_Visibility_aadhar_card_personal, iv_Visibility_driving_licence_personal, iv_Visibility_passport_personal, iv_Visibility_voter_id_personal,
            iv_Visibility_income_tax_returns_personal, iv_Visibility_pan_card_company, iv_Visibility_income_tax_returns_company;
    private ImageView iv_attachment_visibility = null, iv_important_document_visibility = null, iv_kyc_visibility = null;

    private Button btn_finished;

    ArrayList<String> add = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater
                .inflate(R.layout.activity_attachment, container, false);
        initialiseIds(mRootView);

        iv_important_document_visibility = iv_Visibility_Vat_returns;
        iv_attachment_visibility = iv_Visibility_Important_document;

        ll_Important_document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (important_document_layout.getVisibility() == View.VISIBLE) {
                    important_document_layout.setVisibility(View.GONE);
                    iv_attachment_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (view_appear != null) {
                        view_appear.setVisibility(View.GONE);
                        iv_attachment_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    important_document_layout.setVisibility(View.VISIBLE);
                    view_appear = important_document_layout;
                    iv_attachment_visibility = iv_Visibility_Important_document;
                    iv_attachment_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        ll_kyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (kyc_layout.getVisibility() == View.VISIBLE) {
                    kyc_layout.setVisibility(View.GONE);
                    iv_attachment_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (view_appear != null) {
                        view_appear.setVisibility(View.GONE);
                        iv_attachment_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    kyc_layout.setVisibility(View.VISIBLE);
                    view_appear = kyc_layout;
                    iv_attachment_visibility = iv_Visibility_kyc;
                    iv_attachment_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        ll_Vat_returns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vat_returns_layout.getVisibility() == View.VISIBLE) {
                    vat_returns_layout.setVisibility(View.GONE);
                    iv_important_document_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (important_document_view_appear != null) {
                        important_document_view_appear.setVisibility(View.GONE);
                        iv_important_document_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    vat_returns_layout.setVisibility(View.VISIBLE);
                    important_document_view_appear = vat_returns_layout;
                    iv_important_document_visibility = iv_Visibility_Vat_returns;
                    iv_important_document_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        ll_Bank_statement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bank_statement_company_layout.getVisibility() == View.VISIBLE) {
                    bank_statement_company_layout.setVisibility(View.GONE);
                    iv_important_document_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (important_document_view_appear != null) {
                        important_document_view_appear.setVisibility(View.GONE);
                        iv_important_document_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    bank_statement_company_layout.setVisibility(View.VISIBLE);
                    important_document_view_appear = bank_statement_company_layout;
                    iv_important_document_visibility = iv_Visibility_Bank_statement;
                    iv_important_document_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_Bank_statement_director.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bank_statement_director_layout.getVisibility() == View.VISIBLE) {
                    bank_statement_director_layout.setVisibility(View.GONE);
                    iv_important_document_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (important_document_view_appear != null) {
                        important_document_view_appear.setVisibility(View.GONE);
                        iv_important_document_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    bank_statement_director_layout.setVisibility(View.VISIBLE);
                    important_document_view_appear = bank_statement_director_layout;
                    iv_important_document_visibility = iv_Visibility_Bank_statement_director;
                    iv_important_document_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        //FOR KYC VIEW
        ll_Pan_card_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pan_card_personal_layout.getVisibility() == View.VISIBLE) {
                    pan_card_personal_layout.setVisibility(View.GONE);
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (kyc_view_appear != null) {
                        kyc_view_appear.setVisibility(View.GONE);
                        iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    pan_card_personal_layout.setVisibility(View.VISIBLE);
                    kyc_view_appear = pan_card_personal_layout;
                    iv_kyc_visibility = iv_Visibility_Pan_card_personal;
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_aadhar_card_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aadhar_card_personal_layout.getVisibility() == View.VISIBLE) {
                    aadhar_card_personal_layout.setVisibility(View.GONE);
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (kyc_view_appear != null) {
                        kyc_view_appear.setVisibility(View.GONE);
                        iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    aadhar_card_personal_layout.setVisibility(View.VISIBLE);
                    kyc_view_appear = aadhar_card_personal_layout;
                    iv_kyc_visibility = iv_Visibility_aadhar_card_personal;
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_driving_licence_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driving_licence_personal_layout.getVisibility() == View.VISIBLE) {
                    driving_licence_personal_layout.setVisibility(View.GONE);
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (kyc_view_appear != null) {
                        kyc_view_appear.setVisibility(View.GONE);
                        iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    driving_licence_personal_layout.setVisibility(View.VISIBLE);
                    kyc_view_appear = driving_licence_personal_layout;
                    iv_kyc_visibility = iv_Visibility_driving_licence_personal;
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_passport_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passport_personal_layout.getVisibility() == View.VISIBLE) {
                    passport_personal_layout.setVisibility(View.GONE);
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (kyc_view_appear != null) {
                        kyc_view_appear.setVisibility(View.GONE);
                        iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    passport_personal_layout.setVisibility(View.VISIBLE);
                    kyc_view_appear = passport_personal_layout;
                    iv_kyc_visibility = iv_Visibility_passport_personal;
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_voter_id_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (voter_id_personal_layout.getVisibility() == View.VISIBLE) {
                    voter_id_personal_layout.setVisibility(View.GONE);
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (kyc_view_appear != null) {
                        kyc_view_appear.setVisibility(View.GONE);
                        iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    voter_id_personal_layout.setVisibility(View.VISIBLE);
                    kyc_view_appear = voter_id_personal_layout;
                    iv_kyc_visibility = iv_Visibility_voter_id_personal;
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_income_tax_returns_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (income_tax_returns_personal_layout.getVisibility() == View.VISIBLE) {
                    income_tax_returns_personal_layout.setVisibility(View.GONE);
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (kyc_view_appear != null) {
                        kyc_view_appear.setVisibility(View.GONE);
                        iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    income_tax_returns_personal_layout.setVisibility(View.VISIBLE);
                    kyc_view_appear = income_tax_returns_personal_layout;
                    iv_kyc_visibility = iv_Visibility_income_tax_returns_personal;
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_pan_card_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pan_card_company_layout.getVisibility() == View.VISIBLE) {
                    pan_card_company_layout.setVisibility(View.GONE);
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (kyc_view_appear != null) {
                        kyc_view_appear.setVisibility(View.GONE);
                        iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    pan_card_company_layout.setVisibility(View.VISIBLE);
                    kyc_view_appear = pan_card_company_layout;
                    iv_kyc_visibility = iv_Visibility_pan_card_company;
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });
        ll_income_tax_returns_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (income_tax_returns_company_layout.getVisibility() == View.VISIBLE) {
                    income_tax_returns_company_layout.setVisibility(View.GONE);
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                } else {
                    if (kyc_view_appear != null) {
                        kyc_view_appear.setVisibility(View.GONE);
                        iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.expand));
                    }
                    income_tax_returns_company_layout.setVisibility(View.VISIBLE);
                    kyc_view_appear = income_tax_returns_company_layout;
                    iv_kyc_visibility = iv_Visibility_income_tax_returns_company;
                    iv_kyc_visibility.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.collapse));
                }
            }
        });

        btn_vat_returns_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_CODE_VAT_RETURNS);
            }
        });

        btn_bank_statement_company_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_BANK_STATEMENT_COMPANY);
            }
        });

        btn_bank_statement_director_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_BANK_STATEMENT_DIRECTOR);

            }
        });

        btn_pan_card_personal_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_PAN_CARD_PERSONAL);

            }
        });

        btn_aadhar_card_personal_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_AADHAR_CARD_PERSONAL);

            }
        });

        btn_driving_licence_personal_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_DRIVING_LICENCE_PERSONAL);
            }
        });


        btn_passport_personal_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_PASSPORT_PERSONAL);
            }
        });

        btn_voter_id_personal_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_VOTER_ID_PERSONAL);
            }
        });

        btn_income_tax_returns_personal_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_INCOME_TAX_RETURNS_PERSONAL);
            }
        });


        btn_pan_card_company_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_PAN_CARD_COMPANY);
            }
        });


        btn_income_tax_returns_company_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser(UPLOAD_REQ_INCOME_TAX_RETURNS_COMPANY);
            }
        });

        //For getting uploaded data
        btn_finished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("size", "sizes" + Constants.VAT_RETURNS.size());
                Gson gson = new Gson();
                String json = gson.toJson(Constants.mAttachments);
                Log.e("json", "" + json);
            }
        });


        return mRootView;
    }

    private void initialiseIds(View mRootView) {

        ll_Important_document = (LinearLayout) mRootView.findViewById(R.id.ll_Important_document);
        ll_kyc = (LinearLayout) mRootView.findViewById(R.id.ll_kyc);
        ll_Vat_returns = (LinearLayout) mRootView.findViewById(R.id.ll_Vat_returns);
        ll_Bank_statement = (LinearLayout) mRootView.findViewById(R.id.ll_Bank_statement);
        ll_Bank_statement_director = (LinearLayout) mRootView.findViewById(R.id.ll_Bank_statement_director);
        ll_Pan_card_personal = (LinearLayout) mRootView.findViewById(R.id.ll_Pan_card_personal);
        ll_aadhar_card_personal = (LinearLayout) mRootView.findViewById(R.id.ll_aadhar_card_personal);
        ll_driving_licence_personal = (LinearLayout) mRootView.findViewById(R.id.ll_driving_licence_personal);
        ll_passport_personal = (LinearLayout) mRootView.findViewById(R.id.ll_passport_personal);
        ll_voter_id_personal = (LinearLayout) mRootView.findViewById(R.id.ll_voter_id_personal);
        ll_income_tax_returns_personal = (LinearLayout) mRootView.findViewById(R.id.ll_income_tax_returns_personal);
        ll_pan_card_company = (LinearLayout) mRootView.findViewById(R.id.ll_pan_card_company);
        ll_income_tax_returns_company = (LinearLayout) mRootView.findViewById(R.id.ll_income_tax_returns_company);

        important_document_layout = (RelativeLayout) mRootView.findViewById(R.id.important_document_layout);
        kyc_layout = (RelativeLayout) mRootView.findViewById(R.id.kyc_layout);
        vat_returns_layout = (RelativeLayout) mRootView.findViewById(R.id.vat_returns_layout);
        bank_statement_company_layout = (RelativeLayout) mRootView.findViewById(R.id.bank_statement_company_layout);
        bank_statement_director_layout = (RelativeLayout) mRootView.findViewById(R.id.bank_statement_director_layout);
        pan_card_personal_layout = (RelativeLayout) mRootView.findViewById(R.id.pan_card_personal_layout);
        aadhar_card_personal_layout = (RelativeLayout) mRootView.findViewById(R.id.aadhar_card_personal_layout);
        driving_licence_personal_layout = (RelativeLayout) mRootView.findViewById(R.id.driving_licence_personal_layout);
        passport_personal_layout = (RelativeLayout) mRootView.findViewById(R.id.passport_personal_layout);
        voter_id_personal_layout = (RelativeLayout) mRootView.findViewById(R.id.voter_id_personal_layout);
        income_tax_returns_personal_layout = (RelativeLayout) mRootView.findViewById(R.id.income_tax_returns_personal_layout);
        pan_card_company_layout = (RelativeLayout) mRootView.findViewById(R.id.pan_card_company_layout);
        income_tax_returns_company_layout = (RelativeLayout) mRootView.findViewById(R.id.income_tax_returns_company_layout);

        init_VatReturns_view(mRootView);
    }

    public void init_VatReturns_view(View mRootView) {
        btn_vat_returns_upload = (Button) mRootView.findViewById(R.id.btn_vat_returns_upload);
        btn_bank_statement_company_upload = (Button) mRootView.findViewById(R.id.btn_bank_statement_company_upload);
        btn_bank_statement_director_upload = (Button) mRootView.findViewById(R.id.btn_bank_statement_director_upload);
        btn_pan_card_personal_upload = (Button) mRootView.findViewById(R.id.btn_pan_card_personal_upload);
        btn_aadhar_card_personal_upload = (Button) mRootView.findViewById(R.id.btn_aadhar_card_personal_upload);
        btn_driving_licence_personal_upload = (Button) mRootView.findViewById(R.id.btn_driving_licence_personal_upload);
        btn_passport_personal_upload = (Button) mRootView.findViewById(R.id.btn_passport_personal_upload);
        btn_voter_id_personal_upload = (Button) mRootView.findViewById(R.id.btn_voter_id_personal_upload);
        btn_income_tax_returns_personal_upload = (Button) mRootView.findViewById(R.id.btn_income_tax_returns_personal_upload);
        btn_pan_card_company_upload = (Button) mRootView.findViewById(R.id.btn_pan_card_company_upload);
        btn_income_tax_returns_company_upload = (Button) mRootView.findViewById(R.id.btn_income_tax_returns_company_upload);


        ll_Vat_returns_container = (LinearLayout) mRootView.findViewById(R.id.ll_Vat_returns_container);
        ll_bank_statement_company_container = (LinearLayout) mRootView.findViewById(R.id.ll_bank_statement_company_container);
        ll_bank_statement_director_container = (LinearLayout) mRootView.findViewById(R.id.ll_bank_statement_director_container);
        ll_pan_card_personal_container = (LinearLayout) mRootView.findViewById(R.id.ll_pan_card_personal_container);
        ll_aadhar_card_personal_container = (LinearLayout) mRootView.findViewById(R.id.ll_aadhar_card_personal_container);
        ll_driving_licence_personal_container = (LinearLayout) mRootView.findViewById(R.id.ll_driving_licence_personal_container);
        ll_passport_personal_container = (LinearLayout) mRootView.findViewById(R.id.ll_passport_personal_container);
        ll_voter_id_personal_container = (LinearLayout) mRootView.findViewById(R.id.ll_voter_id_personal_container);
        ll_income_tax_returns_personal_container = (LinearLayout) mRootView.findViewById(R.id.ll_income_tax_returns_personal_container);
        ll_pan_card_company_container = (LinearLayout) mRootView.findViewById(R.id.ll_pan_card_company_container);
        ll_income_tax_returns_company_container = (LinearLayout) mRootView.findViewById(R.id.ll_income_tax_returns_company_container);


        /****** @@@@@@@@@@@@@@@@@@ STATUS IMAGE VIEWS @@@@@@@@@@@@@@@@@@@@@@@ *******/
        iv_Status_Important_document = (ImageView) mRootView.findViewById(R.id.iv_Status_Important_document);
        iv_Status_kyc = (ImageView) mRootView.findViewById(R.id.iv_Status_kyc);
        iv_Status_Vat_returns = (ImageView) mRootView.findViewById(R.id.iv_Status_Vat_returns);
        iv_Status_Bank_statement = (ImageView) mRootView.findViewById(R.id.iv_Status_Bank_statement);
        iv_Status_Bank_statement_director = (ImageView) mRootView.findViewById(R.id.iv_Status_Bank_statement_director);
        iv_Status_Pan_card_personal = (ImageView) mRootView.findViewById(R.id.iv_Status_Pan_card_personal);
        iv_Status_aadhar_card_personal = (ImageView) mRootView.findViewById(R.id.iv_Status_aadhar_card_personal);
        iv_Status_driving_licence_personal = (ImageView) mRootView.findViewById(R.id.iv_Status_driving_licence_personal);
        iv_Status_passport_personal = (ImageView) mRootView.findViewById(R.id.iv_Status_passport_personal);
        iv_Status_voter_id_personal = (ImageView) mRootView.findViewById(R.id.iv_Status_voter_id_personal);
        iv_Status_income_tax_returns_personal = (ImageView) mRootView.findViewById(R.id.iv_Status_income_tax_returns_personal);
        iv_Status_pan_card_company = (ImageView) mRootView.findViewById(R.id.iv_Status_pan_card_company);
        iv_Status_income_tax_returns_company = (ImageView) mRootView.findViewById(R.id.iv_Status_income_tax_returns_company);


        /****** @@@@@@@@@@@@@@@@@@ VISIBILITY IMAGE VIEWS @@@@@@@@@@@@@@@@@@@@ ************/
        iv_Visibility_Important_document = (ImageView) mRootView.findViewById(R.id.iv_Visibility_Important_document);
        iv_Visibility_kyc = (ImageView) mRootView.findViewById(R.id.iv_Visibility_kyc);
        iv_Visibility_Vat_returns = (ImageView) mRootView.findViewById(R.id.iv_Visibility_Vat_returns);
        iv_Visibility_Bank_statement = (ImageView) mRootView.findViewById(R.id.iv_Visibility_Bank_statement);
        iv_Visibility_Bank_statement_director = (ImageView) mRootView.findViewById(R.id.iv_Visibility_Bank_statement_director);
        iv_Visibility_Pan_card_personal = (ImageView) mRootView.findViewById(R.id.iv_Visibility_Pan_card_personal);
        iv_Visibility_aadhar_card_personal = (ImageView) mRootView.findViewById(R.id.iv_Visibility_aadhar_card_personal);
        iv_Visibility_driving_licence_personal = (ImageView) mRootView.findViewById(R.id.iv_Visibility_driving_licence_personal);
        iv_Visibility_passport_personal = (ImageView) mRootView.findViewById(R.id.iv_Visibility_passport_personal);
        iv_Visibility_voter_id_personal = (ImageView) mRootView.findViewById(R.id.iv_Visibility_voter_id_personal);
        iv_Visibility_income_tax_returns_personal = (ImageView) mRootView.findViewById(R.id.iv_Visibility_income_tax_returns_personal);
        iv_Visibility_pan_card_company = (ImageView) mRootView.findViewById(R.id.iv_Visibility_pan_card_company);
        iv_Visibility_income_tax_returns_company = (ImageView) mRootView.findViewById(R.id.iv_Visibility_income_tax_returns_company);

        btn_finished = (Button) mRootView.findViewById(R.id.btn_finished);

    }

    private void showFileChooser(int FILE_SELECT_CODE) {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("*/*");
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i = Intent.createChooser(i, "Choose a file");
        startActivityForResult(i, FILE_SELECT_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String path;
        if (resultCode == -1) {
            Log.e("Request Code", "" + requestCode);
            Log.e("Result Code", "" + resultCode);
            path = getRealPathFromURI(data.getData());
            try {
                Log.w("Intent", data.getData().toString());
                Log.e("path", "::" + path);
                switch (requestCode) {
                    case UPLOAD_REQ_CODE_VAT_RETURNS:
                        add_vat_returnsView(path, UPLOAD_REQ_CODE_VAT_RETURNS);
                        break;
                    case UPLOAD_REQ_BANK_STATEMENT_COMPANY:
                        add_bank_statement_companyView(path, UPLOAD_REQ_BANK_STATEMENT_COMPANY);
                        break;
                    case UPLOAD_REQ_BANK_STATEMENT_DIRECTOR:
                        add_bank_statement_directorView(path, UPLOAD_REQ_BANK_STATEMENT_DIRECTOR);
                        break;
                    case UPLOAD_REQ_PAN_CARD_PERSONAL:
                        add_pan_card_personalView(path, UPLOAD_REQ_PAN_CARD_PERSONAL);
                        break;
                    case UPLOAD_REQ_AADHAR_CARD_PERSONAL:
                        add_aadhar_card_personalView(path, UPLOAD_REQ_AADHAR_CARD_PERSONAL);
                        break;
                    case UPLOAD_REQ_DRIVING_LICENCE_PERSONAL:
                        add_driving_licence_personalView(path, UPLOAD_REQ_DRIVING_LICENCE_PERSONAL);
                        break;
                    case UPLOAD_REQ_PASSPORT_PERSONAL:
                        add_passport_personalView(path, UPLOAD_REQ_PASSPORT_PERSONAL);
                        break;
                    case UPLOAD_REQ_VOTER_ID_PERSONAL:
                        add_voter_id_personalView(path, UPLOAD_REQ_VOTER_ID_PERSONAL);
                        break;
                    case UPLOAD_REQ_INCOME_TAX_RETURNS_PERSONAL:
                        add_income_tax_returns_personalView(path, UPLOAD_REQ_INCOME_TAX_RETURNS_PERSONAL);
                        break;
                    case UPLOAD_REQ_PAN_CARD_COMPANY:
                        add_pan_card_companyView(path, UPLOAD_REQ_PAN_CARD_COMPANY);
                        break;
                    case UPLOAD_REQ_INCOME_TAX_RETURNS_COMPANY:
                        add_income_tax_returns_companyView(path, UPLOAD_REQ_INCOME_TAX_RETURNS_COMPANY);
                        break;
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.w("Error", e.toString());
            }
        }
    }


    public String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};

        if ("content".equalsIgnoreCase(contentUri.getScheme())) {
            Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                path = cursor.getString(column_index);
            }
            cursor.close();
            return path;
        } else if ("file".equalsIgnoreCase(contentUri.getScheme())) {
            return contentUri.getPath();
        }
        return null;
    }

    public void viewFile(String file_name) {
        File file = new File(file_name);
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "*/*");
        getActivity().startActivity(intent);
    }

    public void add_vat_returnsView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.VAT_RETURNS);
      /*  add.add(path);
        Attachments ack = new Attachments();
        Attachments.ImportantDocument ack1 = ack.new ImportantDocument();
        ack1.setVatReturn(add);
        ack.setImportantDocument(ack1);
        //  Log.e("all values", "" + ack.getVatreturn().size());
        Gson gson = new Gson();
        String json = gson.toJson(ack);
        Log.e("json", "" + json);*/

        ll_Vat_returns_container.addView(addView);
    }

    private void add_bank_statement_companyView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.BANK_STATEMENT_COMPANY);
        ll_bank_statement_company_container.addView(addView);
    }

    private void add_bank_statement_directorView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.BANK_STATEMENT_DIRECTOR);
        ll_bank_statement_director_container.addView(addView);
    }

    private void add_pan_card_personalView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.PAN_CARD_PERSONAL);
        ll_pan_card_personal_container.addView(addView);
    }

    private void add_aadhar_card_personalView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.AADHAR_CARD_PERSONAL);
        ll_aadhar_card_personal_container.addView(addView);
    }

    private void add_driving_licence_personalView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.DRIVING_LICENCE_PERSONAL);
        ll_driving_licence_personal_container.addView(addView);
    }

    private void add_passport_personalView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.PASSPORT_PERSONAL);
        ll_passport_personal_container.addView(addView);
    }

    private void add_voter_id_personalView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.VOTER_ID_PERSONAL);
        ll_voter_id_personal_container.addView(addView);
    }

    private void add_income_tax_returns_personalView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.INCOME_TAX_RETURNS_PERSONAL);
        ll_income_tax_returns_personal_container.addView(addView);
    }

    private void add_pan_card_companyView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.PAN_CARD_COMPANY);
        ll_pan_card_company_container.addView(addView);
    }

    private void add_income_tax_returns_companyView(final String path, int typeOfUpload) {
        View addView = getUploadView(path, typeOfUpload, Constants.INCOME_TAX_RETURNS_COMPANY);
        ll_income_tax_returns_company_container.addView(addView);
    }

    private View getUploadView(final String path, int typeOfUpload, final ArrayList<String> files) {
        String[] file = path.split("/");
        String file_name = file[file.length - 1];
        LayoutInflater layoutInflater =
                (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.custom_vat_return_list, null);
        RobotoLightTextView tv_vat_returns_file_name = (RobotoLightTextView) addView.findViewById(R.id.tv_vat_returns_file_name);
        tv_vat_returns_file_name.setText("" + file_name);
        final ImageView iv_vat_returns_delete = (ImageView) addView.findViewById(R.id.iv_vat_returns_delete);
        ImageView iv_vat_return_file_view = (ImageView) addView.findViewById(R.id.iv_vat_return_file_view);
        iv_vat_returns_delete.setTag(R.id.typeOfView, "" + typeOfUpload);
        iv_vat_returns_delete.setTag(R.id.fileSize, "" + files.size());
        files.add(path);
        updateData(typeOfUpload);
        iv_vat_return_file_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFile(path);
            }
        });
        iv_vat_returns_delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((LinearLayout) addView.getParent()).removeView(addView);
                int position = Integer.parseInt("" + iv_vat_returns_delete.getTag(R.id.fileSize));
                int type = Integer.parseInt("" + iv_vat_returns_delete.getTag(R.id.typeOfView));
                files.remove(position);
                Log.e("typeOfFile", "" + iv_vat_returns_delete.getTag(R.id.typeOfView));
                Log.e("filesCount", "" + iv_vat_returns_delete.getTag(R.id.fileSize));
                updateData(type);
            }
        });
        return addView;
    }

    public void updateData(int typeOfUpload) {
        Constants.mImportantDocuments.setVatReturn(Constants.VAT_RETURNS);
        Constants.mImportantDocuments.setBankStatementCompany(Constants.BANK_STATEMENT_COMPANY);
        Constants.mImportantDocuments.setBankStatementDirector(Constants.BANK_STATEMENT_DIRECTOR);

        Constants.mAttachments.setImportantDocument(Constants.mImportantDocuments);

        Constants.mKyc.setPanCardPersonal(Constants.PAN_CARD_PERSONAL);
        Constants.mKyc.setAadharCardPersonal(Constants.AADHAR_CARD_PERSONAL);
        Constants.mKyc.setDrivingLicencePersonal(Constants.DRIVING_LICENCE_PERSONAL);
        Constants.mKyc.setPassportPersonal(Constants.PASSPORT_PERSONAL);
        Constants.mKyc.setVoterIdPersonal(Constants.VOTER_ID_PERSONAL);
        Constants.mKyc.setIncomeTaxReturnsPersonal(Constants.INCOME_TAX_RETURNS_PERSONAL);
        Constants.mKyc.setPanCardCompany(Constants.PAN_CARD_COMPANY);
        Constants.mKyc.setIncomeTaxReturnsCompany(Constants.INCOME_TAX_RETURNS_COMPANY);

        Constants.mAttachments.setKyc(Constants.mKyc);

        updateStatusImages(typeOfUpload);

    }

    public void updateStatusImages(int typeOfView) {
        switch (typeOfView) {
            case UPLOAD_REQ_CODE_VAT_RETURNS:
                if (Constants.VAT_RETURNS.size() > 0) {
                    iv_Status_Vat_returns.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_Vat_returns.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_BANK_STATEMENT_COMPANY:
                if (Constants.BANK_STATEMENT_COMPANY.size() > 0) {
                    iv_Status_Bank_statement.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_Bank_statement.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_BANK_STATEMENT_DIRECTOR:
                if (Constants.BANK_STATEMENT_DIRECTOR.size() > 0) {
                    iv_Status_Bank_statement_director.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_Bank_statement_director.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_PAN_CARD_PERSONAL:
                if (Constants.PAN_CARD_PERSONAL.size() > 0) {
                    iv_Status_Pan_card_personal.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_Pan_card_personal.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_AADHAR_CARD_PERSONAL:
                if (Constants.AADHAR_CARD_PERSONAL.size() > 0) {
                    iv_Status_aadhar_card_personal.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_aadhar_card_personal.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_DRIVING_LICENCE_PERSONAL:
                if (Constants.DRIVING_LICENCE_PERSONAL.size() > 0) {
                    iv_Status_driving_licence_personal.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_driving_licence_personal.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_PASSPORT_PERSONAL:
                if (Constants.PASSPORT_PERSONAL.size() > 0) {
                    iv_Status_passport_personal.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_passport_personal.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_VOTER_ID_PERSONAL:
                if (Constants.VOTER_ID_PERSONAL.size() > 0) {
                    iv_Status_voter_id_personal.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_voter_id_personal.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_INCOME_TAX_RETURNS_PERSONAL:
                if (Constants.INCOME_TAX_RETURNS_PERSONAL.size() > 0) {
                    iv_Status_income_tax_returns_personal.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_income_tax_returns_personal.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_PAN_CARD_COMPANY:
                if (Constants.PAN_CARD_COMPANY.size() > 0) {
                    iv_Status_pan_card_company.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_pan_card_company.setImageResource(R.drawable.notstarted);
                }
                break;
            case UPLOAD_REQ_INCOME_TAX_RETURNS_COMPANY:
                if (Constants.INCOME_TAX_RETURNS_COMPANY.size() > 0) {
                    iv_Status_income_tax_returns_company.setImageResource(R.drawable.completed);
                } else {
                    iv_Status_income_tax_returns_company.setImageResource(R.drawable.notstarted);
                }
                break;
        }
        //FOR IMPORTANT DOCUMENT STATUS
        if (Constants.VAT_RETURNS.size() > 0 && Constants.BANK_STATEMENT_COMPANY.size() > 0 && Constants.BANK_STATEMENT_DIRECTOR.size() > 0) {
            iv_Status_Important_document.setImageResource(R.drawable.completed);
        } else if (Constants.VAT_RETURNS.size() > 0 || Constants.BANK_STATEMENT_COMPANY.size() > 0 || Constants.BANK_STATEMENT_DIRECTOR.size() > 0) {
            iv_Status_Important_document.setImageResource(R.drawable.notcompleted);
        } else
            iv_Status_Important_document.setImageResource(R.drawable.notstarted);

        //FOR KYC STATUS
        if (Constants.PAN_CARD_PERSONAL.size() > 0 && Constants.AADHAR_CARD_PERSONAL.size() > 0 && Constants.DRIVING_LICENCE_PERSONAL.size() > 0
                && Constants.PASSPORT_PERSONAL.size() > 0 && Constants.VOTER_ID_PERSONAL.size() > 0 && Constants.INCOME_TAX_RETURNS_PERSONAL.size() > 0
                && Constants.PAN_CARD_COMPANY.size() > 0 && Constants.INCOME_TAX_RETURNS_COMPANY.size() > 0) {
            iv_Status_kyc.setImageResource(R.drawable.completed);
        } else if (Constants.PAN_CARD_PERSONAL.size() > 0 || Constants.AADHAR_CARD_PERSONAL.size() > 0 || Constants.DRIVING_LICENCE_PERSONAL.size() > 0
                || Constants.PASSPORT_PERSONAL.size() > 0 || Constants.VOTER_ID_PERSONAL.size() > 0 || Constants.INCOME_TAX_RETURNS_PERSONAL.size() > 0
                || Constants.PAN_CARD_COMPANY.size() > 0 || Constants.INCOME_TAX_RETURNS_COMPANY.size() > 0) {
            iv_Status_kyc.setImageResource(R.drawable.notcompleted);
        } else
            iv_Status_kyc.setImageResource(R.drawable.notstarted);
    }


}
