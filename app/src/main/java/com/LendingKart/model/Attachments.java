package com.LendingKart.model;

import java.util.ArrayList;

/**
 * Created by chakrabo on 9/28/2015.
 */
public class Attachments {
    public ImportantDocument importantDocument;
    public Kyc kyc;

    public Kyc getKyc() {
        return kyc;
    }

    public void setKyc(Kyc kyc) {
        this.kyc = kyc;
    }

    public class Kyc
    {
        public ArrayList<String> panCardPersonal;
        public ArrayList<String> aadharCardPersonal;
        public ArrayList<String> drivingLicencePersonal;
        public ArrayList<String> passportPersonal;
        public ArrayList<String> voterIdPersonal;
        public ArrayList<String> incomeTaxReturnsPersonal;
        public ArrayList<String> panCardCompany;
        public ArrayList<String> incomeTaxReturnsCompany;

        public ArrayList<String> getPanCardPersonal() {
            return panCardPersonal;
        }

        public void setPanCardPersonal(ArrayList<String> panCardPersonal) {
            this.panCardPersonal = panCardPersonal;
        }

        public ArrayList<String> getAadharCardPersonal() {
            return aadharCardPersonal;
        }

        public void setAadharCardPersonal(ArrayList<String> aadharCardPersonal) {
            this.aadharCardPersonal = aadharCardPersonal;
        }

        public ArrayList<String> getDrivingLicencePersonal() {
            return drivingLicencePersonal;
        }

        public void setDrivingLicencePersonal(ArrayList<String> drivingLicencePersonal) {
            this.drivingLicencePersonal = drivingLicencePersonal;
        }

        public ArrayList<String> getPassportPersonal() {
            return passportPersonal;
        }

        public void setPassportPersonal(ArrayList<String> passportPersonal) {
            this.passportPersonal = passportPersonal;
        }

        public ArrayList<String> getVoterIdPersonal() {
            return voterIdPersonal;
        }

        public void setVoterIdPersonal(ArrayList<String> voterIdPersonal) {
            this.voterIdPersonal = voterIdPersonal;
        }

        public ArrayList<String> getIncomeTaxReturnsPersonal() {
            return incomeTaxReturnsPersonal;
        }

        public void setIncomeTaxReturnsPersonal(ArrayList<String> incomeTaxReturnsPersonal) {
            this.incomeTaxReturnsPersonal = incomeTaxReturnsPersonal;
        }

        public ArrayList<String> getPanCardCompany() {
            return panCardCompany;
        }

        public void setPanCardCompany(ArrayList<String> panCardCompany) {
            this.panCardCompany = panCardCompany;
        }

        public ArrayList<String> getIncomeTaxReturnsCompany() {
            return incomeTaxReturnsCompany;
        }

        public void setIncomeTaxReturnsCompany(ArrayList<String> incomeTaxReturnsCompany) {
            this.incomeTaxReturnsCompany = incomeTaxReturnsCompany;
        }
    }

    public ImportantDocument getImportantDocument() {
        return importantDocument;
    }

    public void setImportantDocument(ImportantDocument importantDocument) {
        this.importantDocument = importantDocument;
    }

    public class ImportantDocument {
        public ArrayList<String> vatReturn;
        public ArrayList<String> bankStatementCompany;
        public ArrayList<String> bankStatementDirector;

        public ArrayList<String> getVatReturn() {
            return vatReturn;
        }

        public void setVatReturn(ArrayList<String> vatReturn) {
            this.vatReturn = vatReturn;
        }

        public ArrayList<String> getBankStatementCompany() {
            return bankStatementCompany;
        }

        public void setBankStatementCompany(ArrayList<String> bankStatementCompany) {
            this.bankStatementCompany = bankStatementCompany;
        }

        public ArrayList<String> getBankStatementDirector() {
            return bankStatementDirector;
        }

        public void setBankStatementDirector(ArrayList<String> bankStatementDirector) {
            this.bankStatementDirector = bankStatementDirector;
        }
    }
}
