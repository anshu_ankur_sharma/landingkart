package com.LendingKart.model;

/**
 * Created by chakrabo on 10/1/2015.
 */
public class Director {
    public YourPersonalDetails yourPersonalDetails;

    public class YourPersonalDetails {
        public String FullName;
        public String MobileNumber;
        public String Email;
        public String Dob;

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String fullName) {
            FullName = fullName;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            MobileNumber = mobileNumber;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getDob() {
            return Dob;
        }

        public void setDob(String dob) {
            Dob = dob;
        }
    }

    public AnnualIncomeDetails annualIncomeDetails;

    public class AnnualIncomeDetails {
        public String IncomeAmount;
        public String PanCardAddress;

        public String getIncomeAmount() {
            return IncomeAmount;
        }

        public void setIncomeAmount(String incomeAmount) {
            IncomeAmount = incomeAmount;
        }

        public String getPanCardAddress() {
            return PanCardAddress;
        }

        public void setPanCardAddress(String panCardAddress) {
            PanCardAddress = panCardAddress;
        }
    }

    public PersonalAddress personalAddress;

    public class PersonalAddress {
        public String BusinessAddressPinCode;
        public String BusinessAddressHouseNo;
        public String BusinessAddressCity;
        public String BusinessAddressState;
        public String BusinessResidentialPremises;

        public String PersonalAddressPinCode;
        public String PersonalAddressHouseNo;
        public String PersonalAddressCity;
        public String PersonalAddressState;
        public String PersonalResidentialPremises;

        public String getBusinessAddressPinCode() {
            return BusinessAddressPinCode;
        }

        public void setBusinessAddressPinCode(String businessAddressPinCode) {
            BusinessAddressPinCode = businessAddressPinCode;
        }

        public String getBusinessAddressHouseNo() {
            return BusinessAddressHouseNo;
        }

        public void setBusinessAddressHouseNo(String businessAddressHouseNo) {
            BusinessAddressHouseNo = businessAddressHouseNo;
        }

        public String getBusinessAddressCity() {
            return BusinessAddressCity;
        }

        public void setBusinessAddressCity(String businessAddressCity) {
            BusinessAddressCity = businessAddressCity;
        }

        public String getBusinessAddressState() {
            return BusinessAddressState;
        }

        public void setBusinessAddressState(String businessAddressState) {
            BusinessAddressState = businessAddressState;
        }

        public String getBusinessResidentialPremises() {
            return BusinessResidentialPremises;
        }

        public void setBusinessResidentialPremises(String businessResidentialPremises) {
            BusinessResidentialPremises = businessResidentialPremises;
        }

        public String getPersonalAddressPinCode() {
            return PersonalAddressPinCode;
        }

        public void setPersonalAddressPinCode(String personalAddressPinCode) {
            PersonalAddressPinCode = personalAddressPinCode;
        }

        public String getPersonalAddressHouseNo() {
            return PersonalAddressHouseNo;
        }

        public void setPersonalAddressHouseNo(String personalAddressHouseNo) {
            PersonalAddressHouseNo = personalAddressHouseNo;
        }

        public String getPersonalAddressCity() {
            return PersonalAddressCity;
        }

        public void setPersonalAddressCity(String personalAddressCity) {
            PersonalAddressCity = personalAddressCity;
        }

        public String getPersonalAddressState() {
            return PersonalAddressState;
        }

        public void setPersonalAddressState(String personalAddressState) {
            PersonalAddressState = personalAddressState;
        }

        public String getPersonalResidentialPremises() {
            return PersonalResidentialPremises;
        }

        public void setPersonalResidentialPremises(String personalResidentialPremises) {
            PersonalResidentialPremises = personalResidentialPremises;
        }
    }

    public EducationDetails educationDetails;

    public class EducationDetails {
        public String Education;

        public String getEducation() {
            return Education;
        }

        public void setEducation(String education) {
            Education = education;
        }
    }

    public MyFamilyDetails myFamilyDetails;

    public class MyFamilyDetails {
        public String MaritalStatus;

        public String getMaritalStatus() {
            return MaritalStatus;
        }

        public void setMaritalStatus(String maritalStatus) {
            MaritalStatus = maritalStatus;
        }
    }

    public SocialDetails socialDetails;

    public class SocialDetails {
        public String FaceBook;
        public String LinkedIn;
        public String Twitter;
        public String Others;

        public String getFaceBook() {
            return FaceBook;
        }

        public void setFaceBook(String faceBook) {
            FaceBook = faceBook;
        }

        public String getLinkedIn() {
            return LinkedIn;
        }

        public void setLinkedIn(String linkedIn) {
            LinkedIn = linkedIn;
        }

        public String getTwitter() {
            return Twitter;
        }

        public void setTwitter(String twitter) {
            Twitter = twitter;
        }

        public String getOthers() {
            return Others;
        }

        public void setOthers(String others) {
            Others = others;
        }
    }

    public YourPersonalDetails getYourPersonalDetails() {
        return yourPersonalDetails;
    }

    public void setYourPersonalDetails(YourPersonalDetails yourPersonalDetails) {
        this.yourPersonalDetails = yourPersonalDetails;
    }

    public AnnualIncomeDetails getAnnualIncomeDetails() {
        return annualIncomeDetails;
    }

    public void setAnnualIncomeDetails(AnnualIncomeDetails annualIncomeDetails) {
        this.annualIncomeDetails = annualIncomeDetails;
    }

    public PersonalAddress getPersonalAddress() {
        return personalAddress;
    }

    public void setPersonalAddress(PersonalAddress personalAddress) {
        this.personalAddress = personalAddress;
    }

    public EducationDetails getEducationDetails() {
        return educationDetails;
    }

    public void setEducationDetails(EducationDetails educationDetails) {
        this.educationDetails = educationDetails;
    }

    public MyFamilyDetails getMyFamilyDetails() {
        return myFamilyDetails;
    }

    public void setMyFamilyDetails(MyFamilyDetails myFamilyDetails) {
        this.myFamilyDetails = myFamilyDetails;
    }

    public SocialDetails getSocialDetails() {
        return socialDetails;
    }

    public void setSocialDetails(SocialDetails socialDetails) {
        this.socialDetails = socialDetails;
    }
}
