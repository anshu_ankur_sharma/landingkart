package com.LendingKart.model;

import java.util.ArrayList;

/**
 * Created by sharmaan on 20-09-2015.
 */


public class BusinessDetail {

    String companyName;
    String contactNumber;
    String emailId;
    int numberOfEmployee;
    String companyRegisteredAs;
    ArrayList<ProductSell> productSells;
    ArrayList<BusinessNature> businessNatures;
    String turnOver;
    String companyPanNumber;
    String companyTanNumber;
    boolean expemtedVAT;
    boolean sellServices;
    boolean sellOffline;
    boolean sellOnline;
    String offlineBusinessDate;
    String onlineBusinessDate;
    String businessPremises;
    String pincode;
    String houseNumber;
    String street;
    String city;
    boolean premisesAddressSameAsBusiness;
    ArrayList<SellingOn> sellingOn;
    String bankName;
    boolean ODFacility;
    boolean CCFacilty;
    String percentageCompletion;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public int getNumberOfEmployee() {
        return numberOfEmployee;
    }

    public void setNumberOfEmployee(int numberOfEmployee) {
        this.numberOfEmployee = numberOfEmployee;
    }

    public String getCompanyRegisteredAs() {
        return companyRegisteredAs;
    }

    public void setCompanyRegisteredAs(String companyRegisteredAs) {
        this.companyRegisteredAs = companyRegisteredAs;
    }

    public ArrayList<ProductSell> getProductSells() {
        return productSells;
    }

    public void setProductSells(ArrayList<ProductSell> productSells) {
        this.productSells = productSells;
    }

    public ArrayList<BusinessNature> getBusinessNatures() {
        return businessNatures;
    }

    public void setBusinessNatures(ArrayList<BusinessNature> businessNatures) {
        this.businessNatures = businessNatures;
    }

    public String getTurnOver() {
        return turnOver;
    }

    public void setTurnOver(String turnOver) {
        this.turnOver = turnOver;
    }

    public String getCompanyPanNumber() {
        return companyPanNumber;
    }

    public void setCompanyPanNumber(String companyPanNumber) {
        this.companyPanNumber = companyPanNumber;
    }

    public String getCompanyTanNumber() {
        return companyTanNumber;
    }

    public void setCompanyTanNumber(String companyTanNumber) {
        this.companyTanNumber = companyTanNumber;
    }

    public boolean isExpemtedVAT() {
        return expemtedVAT;
    }

    public void setExpemtedVAT(boolean expemtedVAT) {
        this.expemtedVAT = expemtedVAT;
    }

    public boolean isSellServices() {
        return sellServices;
    }

    public void setSellServices(boolean sellServices) {
        this.sellServices = sellServices;
    }

    public boolean isSellOffline() {
        return sellOffline;
    }

    public void setSellOffline(boolean sellOffline) {
        this.sellOffline = sellOffline;
    }

    public boolean isSellOnline() {
        return sellOnline;
    }

    public void setSellOnline(boolean sellOnline) {
        this.sellOnline = sellOnline;
    }

    public String getOfflineBusinessDate() {
        return offlineBusinessDate;
    }

    public void setOfflineBusinessDate(String offlineBusinessDate) {
        this.offlineBusinessDate = offlineBusinessDate;
    }

    public String getOnlineBusinessDate() {
        return onlineBusinessDate;
    }

    public void setOnlineBusinessDate(String onlineBusinessDate) {
        this.onlineBusinessDate = onlineBusinessDate;
    }

    public String getBusinessPremises() {
        return businessPremises;
    }

    public void setBusinessPremises(String businessPremises) {
        this.businessPremises = businessPremises;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isPremisesAddressSameAsBusiness() {
        return premisesAddressSameAsBusiness;
    }

    public void setPremisesAddressSameAsBusiness(boolean premisesAddressSameAsBusiness) {
        this.premisesAddressSameAsBusiness = premisesAddressSameAsBusiness;
    }

    public ArrayList<SellingOn> getSellingOn() {
        return sellingOn;
    }

    public void setSellingOn(ArrayList<SellingOn> sellingOn) {
        this.sellingOn = sellingOn;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public boolean isODFacility() {
        return ODFacility;
    }

    public void setODFacility(boolean ODFacility) {
        this.ODFacility = ODFacility;
    }

    public boolean isCCFacilty() {
        return CCFacilty;
    }

    public void setCCFacilty(boolean CCFacilty) {
        this.CCFacilty = CCFacilty;
    }

    public String getPercentageCompletion() {
        return percentageCompletion;
    }

    public void setPercentageCompletion(String percentageCompletion) {
        this.percentageCompletion = percentageCompletion;
    }
}
