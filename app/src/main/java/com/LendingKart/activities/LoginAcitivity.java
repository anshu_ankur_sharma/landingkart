package com.LendingKart.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.LendingKart.R;
import com.LendingKart.customviews.LoginEditText;
import com.LendingKart.util.Constants;

/**
 * Created by sharmaan on 12-09-2015.
 */

public class LoginAcitivity extends Activity {
    private LinearLayout llLoginbutton;
    private RelativeLayout rlLoginbutton;
    private ImageView ivLogin, ivLoginMain, ivRegister, ivMainlogo, ivSublogo;
    private LoginEditText etUserName, etPassword;
    TextView tvLogin;
    private int backPress = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialiseIds();
        etUserName.setKeyImeChangeListener(new LoginEditText.KeyImeChange() {

            @Override
            public void onKeyIme(int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    normalView();
                }
            }
        });

        etUserName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        normalView();
                        break;
                }
                return true;
            }
        });
        etPassword.setKeyImeChangeListener(new LoginEditText.KeyImeChange() {

            @Override
            public void onKeyIme(int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    normalView();
                }
            }
        });

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        normalView();
                        break;
                }
                return true;
            }
        });
    }

    public void normalView() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        zoomView(rlLoginbutton);
        rlLoginbutton.setVisibility(View.VISIBLE);
        llLoginbutton.setVisibility(View.GONE);
        stopZoomView(llLoginbutton);
        stopZoomView(ivSublogo);
        ivSublogo.setVisibility(View.GONE);
        zoomView(ivMainlogo);
        ivMainlogo.setVisibility(View.VISIBLE);
    }

    public void loginView() {
        backPress = 0;
        rlLoginbutton.setVisibility(View.GONE);
        llLoginbutton.setVisibility(View.VISIBLE);
        stopZoomView(rlLoginbutton);
        stopZoomView(ivMainlogo);
        zoomView(llLoginbutton);
        zoomView(ivSublogo);
        ivSublogo.setVisibility(View.VISIBLE);
        ivMainlogo.setVisibility(View.GONE);
        etUserName.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(etUserName, InputMethodManager.SHOW_IMPLICIT);

    }

    public void zoomView(View view) {
        ScaleAnimation scal = new ScaleAnimation(0, 1f, 0, 1f, Animation.RELATIVE_TO_SELF, (float) 0.5, Animation.RELATIVE_TO_SELF, (float) 0.5);
        scal.setDuration(500);
        scal.setFillAfter(true);
        view.setAnimation(scal);
    }

    public void stopZoomView(View view) {
        if (view.getAnimation() != null)
            view.getAnimation().cancel();
    }

    private void initialiseIds() {

        llLoginbutton = (LinearLayout) findViewById(R.id.llLoginbutton);
        rlLoginbutton = (RelativeLayout) findViewById(R.id.rlLoginbutton);

        ivLogin = (ImageView) findViewById(R.id.ivLogin);
        ivLoginMain = (ImageView) findViewById(R.id.ivLoginMain);
        ivRegister = (ImageView) findViewById(R.id.ivRegister);
        ivMainlogo = (ImageView) findViewById(R.id.ivMainlogo);
        ivSublogo = (ImageView) findViewById(R.id.ivSublogo);
        tvLogin = (TextView) findViewById(R.id.tvLogin);

        etUserName = (LoginEditText) findViewById(R.id.etUserName);
        etPassword = (LoginEditText) findViewById(R.id.etPassword);

        ivLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginView();
            }
        });
        ivLoginMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callHomePage(0);
            }
        });
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callHomePage(0);
            }
        });

    }

    public void onBackPressed() {
        Log.e("call", "call");
        backPress++;
        if (backPress == 2) {
            super.onBackPressed();
            Log.e("inside", "inside" + backPress);
        }


    }

    private void callHomePage(int value) {
        Intent intent = new Intent(this, HomeLandingActivity.class);
        intent.putExtra(Constants.NAVIGATION_VALUE, value);
        startActivity(intent);
        this.finish();
    }
}
