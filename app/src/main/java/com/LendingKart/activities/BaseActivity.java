package com.LendingKart.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.LendingKart.R;
import com.LendingKart.customviews.TopBarNavigationView;
import com.LendingKart.util.Utils;

/**
 * Created by sharmaan on 07-09-2015.
 */
public class BaseActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        super.onCreate(savedInstanceState);
    }

    /**
     * Method to set actionbar title.
     * call after setSupportActionBar.
     *
     * @param toolbar Toolbar
     * @param title   Title to set.
     */
    protected void setupActionBarTitleWithFont(Toolbar toolbar, String title) {
        if (toolbar == null) {
            return;
        }


        TextView actionbarTitle = (TextView) toolbar.findViewById(R.id.custom_toolbar_title);
        if (!Utils.isEmpty(title)) {
            actionbarTitle.setText(title);
            actionbarTitle.setEllipsize(null);
        }
    }

    /**
     * Method to set actionbar subtitle.
     * call after setSupportActionBar.
     *
     * @param toolbar  Toolbar
     * @param subTitle Subtitle to set.
     */
    protected void setupActionBarSubtitleWithFont(Toolbar toolbar, String subTitle) {
        if (toolbar == null) {
            return;
        }
        TextView actionbarSubtitle = (TextView) toolbar.findViewById(R.id.custom_toolbar_sub_title);
        if (!Utils.isEmpty(subTitle)) {
            actionbarSubtitle.setVisibility(View.VISIBLE);

            actionbarSubtitle.setEllipsize(null);
        } else {
            actionbarSubtitle.setVisibility(View.GONE);
        }
    }

    protected void setHomePageToolBar(Toolbar toolbar, int value) {
        LinearLayout toolbarOptionContainer = (LinearLayout) toolbar.findViewById(R.id
                .toolbar_option_container);
        TopBarNavigationView navigationView = new TopBarNavigationView(this);
        navigationView.setToolbarOptionView(this, value);
        toolbarOptionContainer.addView(navigationView);
    }

    //Override in activity where child fragments can refresh the title.
    public void refreshTitle(String titleName) {
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
