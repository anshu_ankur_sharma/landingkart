package com.LendingKart.activities;


import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.LendingKart.R;
import com.LendingKart.customviews.NavigationDrawer;
import com.LendingKart.fragment.AttachmentFragment;
import com.LendingKart.fragment.BusinessDetailFragment;
import com.LendingKart.fragment.PersonalDetailFragment;
import com.LendingKart.fragment.PersonalDetailsFragmentNew;
import com.LendingKart.util.Constants;

import static android.view.Gravity.START;

/**
 * Created by sharmaan on 07-09-2015.
 */
public class MainDetailActivity extends Activity implements View.OnClickListener {

    NavigationDrawer nhNavigationDrawer;
    DrawerLayout drawerLayout;
    ImageView ivBusinessDetail, ivPersonalDetail, ivAttachment, ivHome, ivHamburger;
    FrameLayout flBusinessLayout, flPersonalDetailLayout, flAttachmentLayout;
    TextView tvActionBarTitle;

    BusinessDetailFragment mBusinessDetailFragment;
    PersonalDetailFragment mPersonalDetailFragment;
    AttachmentFragment mAttachmentFragment;
    FragmentManager mFragmentManager;

    RelativeLayout rl_drawer_home, rl_drawer_my_application, rl_drawer_password_change,
            rl_drawer_profile, rl_drawer_settings, rl_drawer_about, rl_drawer_logout;
    RelativeLayout rlBusinessDetail, rlPersonalDetail, rlAttachMent, rlHome, rlHamburger;
    int mInentValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getFragmentManager();
        setContentView(R.layout.activity_main);

        mInentValue = getIntent().getIntExtra(Constants.INTENT_VALUE, mInentValue);
        initializeIds();


        rlHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeLandingActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ivHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeLandingActivity.class);
                startActivity(intent);
                finish();
            }
        });

        rlBusinessDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadBusinessDetailFragment();
            }
        });

        ivBusinessDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadBusinessDetailFragment();
            }
        });

        rlPersonalDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPersonalDetailFragment();
            }
        });

        ivPersonalDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPersonalDetailFragment();
            }
        });
        ivHamburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        rlAttachMent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadAttachmentFragment();
            }
        });

        ivAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadAttachmentFragment();
            }
        });

        switch (mInentValue) {
            case 0:
            case 1:
                loadBusinessDetailFragment();
                break;
            case 2:

                loadPersonalDetailFragment();
                break;

            case 3:
                loadAttachmentFragment();
                break;

            default:
                loadBusinessDetailFragment();
                break;
        }
    }

    private void initializeIds() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ivBusinessDetail = (ImageView) findViewById(R.id.ivBusinessDetail);
        ivPersonalDetail = (ImageView) findViewById(R.id.ivPersonalDetail);
        ivAttachment = (ImageView) findViewById(R.id.ivAttachMent);
        ivHome = (ImageView) findViewById(R.id.ivHome);
        ivHamburger = (ImageView) findViewById(R.id.ivHamBurger);

        tvActionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);

        flBusinessLayout = (FrameLayout) findViewById(R.id.flBusinessFragment);
        flPersonalDetailLayout = (FrameLayout) findViewById(R.id.flPersonalFragment);
        flAttachmentLayout = (FrameLayout) findViewById(R.id.flAttachMentFragment);

        rlBusinessDetail = (RelativeLayout) findViewById(R.id.rlBusinessDetail);
        rlPersonalDetail = (RelativeLayout) findViewById(R.id.rlPersonalDetail);
        rlAttachMent = (RelativeLayout) findViewById(R.id.rlAttachMent);
        rlHome = (RelativeLayout) findViewById(R.id.rlHome);


        mBusinessDetailFragment = new BusinessDetailFragment();
        mPersonalDetailFragment = new PersonalDetailFragment();
        mAttachmentFragment = new AttachmentFragment();

        rl_drawer_home = (RelativeLayout) findViewById(R.id.rl_drawer_home);
        rl_drawer_my_application = (RelativeLayout) findViewById(R.id.rl_drawer_my_application);
        rl_drawer_password_change = (RelativeLayout) findViewById(R.id.rl_drawer_password_change);
        rl_drawer_profile = (RelativeLayout) findViewById(R.id.rl_drawer_profile);
        rl_drawer_settings = (RelativeLayout) findViewById(R.id.rl_drawer_settings);
        rl_drawer_about = (RelativeLayout) findViewById(R.id.rl_drawer_about);
        rl_drawer_logout = (RelativeLayout) findViewById(R.id.rl_drawer_logout);

        rl_drawer_home.setOnClickListener(this);
        rl_drawer_my_application.setOnClickListener(this);
        rl_drawer_password_change.setOnClickListener(this);
        rl_drawer_profile.setOnClickListener(this);
        rl_drawer_settings.setOnClickListener(this);
        rl_drawer_about.setOnClickListener(this);
        rl_drawer_logout.setOnClickListener(this);


    }

    private void loadBusinessDetailFragment() {

        tvActionBarTitle.setText(getResources().getString(R.string.busndettxt));
        ivBusinessDetail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.businessdetail_select));
        ivPersonalDetail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.personaldetail));
        ivAttachment.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.attachment));
        flBusinessLayout.setVisibility(View.VISIBLE);
        flPersonalDetailLayout.setVisibility(View.GONE);
        flAttachmentLayout.setVisibility(View.GONE);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flBusinessFragment, mBusinessDetailFragment);
        fragmentTransaction.commit();
        mFragmentManager.executePendingTransactions();

    }

    private void loadPersonalDetailFragment() {

        tvActionBarTitle.setText(getResources().getString(R.string.persndettxt));
        ivBusinessDetail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.businessdetails));
        ivPersonalDetail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.personals_select));
        ivAttachment.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.attachment));
        flBusinessLayout.setVisibility(View.GONE);
        flPersonalDetailLayout.setVisibility(View.VISIBLE);
        flAttachmentLayout.setVisibility(View.GONE);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flPersonalFragment, mPersonalDetailFragment);
        fragmentTransaction.commit();
        mFragmentManager.executePendingTransactions();

    }

    private void loadAttachmentFragment() {

        tvActionBarTitle.setText(getResources().getString(R.string.attachtxt));
        ivBusinessDetail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.businessdetails));
        ivPersonalDetail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.personaldetail));
        ivAttachment.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.attachments_select));
        flBusinessLayout.setVisibility(View.GONE);
        flPersonalDetailLayout.setVisibility(View.GONE);
        flAttachmentLayout.setVisibility(View.VISIBLE);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flAttachMentFragment, mAttachmentFragment);
        fragmentTransaction.commit();
        mFragmentManager.executePendingTransactions();

    }

    public void onBackPressed() {
        // super.onBackPressed();
        activityClose();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_drawer_home:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.rl_drawer_my_application:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.rl_drawer_password_change:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.rl_drawer_profile:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.rl_drawer_settings:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.rl_drawer_about:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.rl_drawer_logout:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
        }
    }

    public void activityClose() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.appclose, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        Button btnOk = (Button) dialogView.findViewById(R.id.btnOk);
        Button btnCancel = (Button) dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }
}
