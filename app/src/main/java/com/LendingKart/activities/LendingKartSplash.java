package com.LendingKart.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.LendingKart.R;
import com.LendingKart.util.Utils;

/**
 *
 *  Created by sharmaan on 07-09-2015.
 */

public class LendingKartSplash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //for displaying splash in full screen mode.
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_kart_splash);
        callThreadMethod();
    }

    protected void callThreadMethod() {
        // TODO Auto-generated method stub

        Thread timer = new Thread() {
            public void run() {
                try {
                    // wait for 2000 milliseconds
                    // it will keep the screen for 3 seconds
                    sleep(3000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    callNextActivityLevel();
                }

            }

        };
        // to start the thread
        timer.start();

    }

    protected void callNextActivityLevel() {
        // TODO Auto-generated method stub

        if (Utils.hasConnection(this)) {
            Intent intent = new Intent(this,
                    LoginAcitivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            overridePendingTransition(0, 0);
            this.finish();
        } else {
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_landing_kart_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
