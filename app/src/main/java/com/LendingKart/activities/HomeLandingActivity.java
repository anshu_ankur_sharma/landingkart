package com.LendingKart.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.LendingKart.R;
import com.LendingKart.customviews.CircularProgress;
import com.LendingKart.util.AppPreferences;
import com.LendingKart.util.Constants;

/**
 * Created by sharmaan on 12-09-2015.
 */
public class HomeLandingActivity extends Activity {

    RelativeLayout rlBusinessLayout, rlPersonalLayout, rlAttachLayout;
    CircularProgress cpBusinessDetail, cpPersonalDetail, cpAttachment;
    int value = 0;
    AppPreferences appPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        value = getIntent().getIntExtra(Constants.NAVIGATION_VALUE, value);
        rlAttachLayout = (RelativeLayout) findViewById(R.id.rlAttachLayout);
        rlBusinessLayout = (RelativeLayout) findViewById(R.id.rlBusineesLayout);
        rlPersonalLayout = (RelativeLayout) findViewById(R.id.rlProgressLayout);
        appPref = new AppPreferences(this);
        cpBusinessDetail = (CircularProgress) findViewById(R.id.cpBusinessProgreess);
        cpPersonalDetail = (CircularProgress) findViewById(R.id.cpPersonalProgress);
        cpAttachment = (CircularProgress) findViewById(R.id.cpAttachProgress);

        cpBusinessDetail.setProgress(appPref.getBusinessdetailPercentage());
        cpPersonalDetail.setProgress(appPref.getPersonaldetailPercentage());
        cpAttachment.setProgress(appPref.getAttachmnetPercentage());


        rlBusinessLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callIntent(1);
            }
        });

        rlPersonalLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callIntent(2);
            }
        });

        rlAttachLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callIntent(3);
            }
        });
    }

    private void callIntent(int i) {

        Intent intent = new Intent(this, MainDetailActivity.class);
        intent.putExtra(Constants.INTENT_VALUE, i);
        startActivity(intent);
        this.finish();
    }
}

